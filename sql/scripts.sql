
CREATE TABLE ALBUM (
  ID int(11) NOT NULL auto_increment,
  TITLE varchar(100)NOT NULL,

  PRIMARY KEY  (ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=73 ;


CREATE TABLE ARTIST (
  ID int(11) NOT NULL auto_increment,
  NAME varchar(100) NOT NULL,
  PRIMARY KEY  (ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=60 ;


-- 
-- Estructura de tabla para la tabla `artistalbum`
-- 

CREATE TABLE ARTIST_ALBUM (
  ALBUM_ID int(11) NOT NULL,
  ARTIST_ID int(11) NOT NULL,
  PRIMARY KEY  (ALBUM_ID,ARTIST_ID),
  KEY ALBUM_ID (ALBUM_ID),
  KEY ARTIST_ID (ARTIST_ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;



-- 
-- Estructura de tabla para la tabla `book`
-- 

CREATE TABLE BOOK (
  ID int(11) NOT NULL auto_increment,
  TITLE varchar(100) NOT NULL,

  PRIMARY KEY  (ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=25 ;


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `bookcommentrev`
-- 

CREATE TABLE BOOK_COMMENT_REV (
  ID int(11) NOT NULL auto_increment,
  USER_ID int(11) NOT NULL,
  BOOK_ID int(11) NOT NULL,
  BOOK_COMMENT text,
  COMMIT_COMMENT varchar(255) default '',
  DATE varchar(50) default NULL,
  TIME timestamp default CURRENT_TIMESTAMP,
  PRIMARY KEY  (ID),
  KEY BOOK_ID (BOOK_ID),
  KEY USER_ID (USER_ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1 ;


-- 
-- Estructura de tabla para la tabla `bookrev`
-- 

CREATE TABLE BOOK_REV (
  ID int(11) NOT NULL auto_increment,
  BOOK_ID int(11) NOT NULL,
  USER_ID int(11) NOT NULL,
  DATE varchar(50) NOT NULL,
  DESCRIPTION text,
  COMMIT_COMMENT varchar(255)default '',
  TIME timestamp default CURRENT_TIMESTAMP,
  PRIMARY KEY  (ID),
  KEY BOOK_ID (BOOK_ID),
  KEY USER_ID (USER_ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=59 ;

-- 
-- Estructura de tabla para la tabla `booktab`
-- 

CREATE TABLE BOOK_TAB (
  BOOK_REV_ID int(11) NOT NULL auto_increment,
  TAB_ID int(11) NOT NULL,
  NOTATION varchar(10) default NULL,
  TONE varchar(10) default NULL,
  PRIMARY KEY  (BOOK_REV_ID,TAB_ID),
  KEY BOOK_REV_ID (BOOK_REV_ID),
  KEY TAB_ID (TAB_ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `chord`
-- 

CREATE TABLE CHORD (
  NAME varchar(10) NOT NULL,
  DEFINITION varchar(50) NOT NULL,
  PRIMARY KEY  (NAME)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `commentrev`
-- 

CREATE TABLE COMMENT_REV (
  ID int(11) NOT NULL auto_increment,
  TAB_ID int(11) NOT NULL,
  USER_ID int(11) NOT NULL,
  COMMENT text NOT NULL,
  COMMIT_COMMENT varchar(255) default '',
  DATE varchar(50) NOT NULL default '',
  TIME timestamp default CURRENT_TIMESTAMP,
  PRIMARY KEY  (ID),
  KEY TAB_ID (TAB_ID),
  KEY USER_ID (USER_ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=109 ;



-- 
-- Estructura de tabla para la tabla `rememberme`
-- 

CREATE TABLE REMEMBER_ME (
  TOKEN varchar(32) NOT NULL,
  NAME varchar(50) NOT NULL,
  PRIMARY KEY  (TOKEN)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


-- 
-- Estructura de tabla para la tabla `song`
-- 

CREATE TABLE SONG (
  ID int(11) NOT NULL auto_increment,
  TITLE varchar(100) NOT NULL,
  PRIMARY KEY  (ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=259 ;



-- 
-- Estructura de tabla para la tabla `tab`
-- 

CREATE TABLE TAB (
  ID int(11) NOT NULL auto_increment,
  SONG_ID int(11) NOT NULL,
  PRIMARY KEY  (ID),
  KEY SONG_ID (SONG_ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=265 ;


-- 
-- Estructura de tabla para la tabla `tabrev`
-- 

CREATE TABLE TAB_REV (
  ID int(11) NOT NULL auto_increment,
  TAB_ID int(11) NOT NULL,
  USER_ID int(11) NOT NULL,
  TAB text NOT NULL,
  NOTATION varchar(10)NOT NULL default 'english',
  DATE varchar(50) NOT NULL default '',
  COMMIT_COMMENT varchar(255) default '',
  TIME timestamp default CURRENT_TIMESTAMP,
  PRIMARY KEY  (ID),
  KEY TAB_ID (TAB_ID),
  KEY USER_ID (USER_ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=811 ;



-- 
-- Estructura de tabla para la tabla `tabrevalbum`
-- 

CREATE TABLE TAB_REV_ALBUM (
  TAB_REV_ID int(11) NOT NULL,
  ALBUM_ID int(11) NOT NULL,
  PRIMARY KEY  (TAB_REV_ID,ALBUM_ID),
  KEY ALBUM_ID (ALBUM_ID),
  KEY TAB_REV_ID (TAB_REV_ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;



-- 
-- Estructura de tabla para la tabla `tabrevperformer`
-- 

CREATE TABLE TAB_REV_ARTIST (
  TAB_REV_ID int(11) NOT NULL,
  ARTIST_ID int(11) NOT NULL,
  PRIMARY KEY  (TAB_REV_ID,ARTIST_ID),
  KEY ARTIST_ID (ARTIST_ID),
  KEY TAB_REV_ID (TAB_REV_ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


-- 
-- Estructura de tabla para la tabla `user`
-- 

CREATE TABLE USER (
  ID int(11) NOT NULL auto_increment,
  USERNAME varchar(50) NOT NULL,
  PASS varchar(32) NOT NULL,
  EMAIL varchar(50) default NULL,
  ADMIN tinyint(4) NOT NULL default '0',
  NOT_REGISTER tinyint(4) NOT NULL default '0',
  BANNED tinyint(4) NOT NULL default '0',
  DESCRIPTION text,
  REGISTER_DATE timestamp NOT NULL default CURRENT_TIMESTAMP,
  IP varchar(16) NOT NULL default '0.0.0.0',
  PRIMARY KEY  (ID),
  UNIQUE KEY USERNAME (USERNAME)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=666 ;



-- 
-- Estructura de tabla para la tabla `userfavoritebook`
-- 

CREATE TABLE USER_FAVORITE_BOOK (
  USER_ID int(11) NOT NULL,
  BOOK_ID int(11) NOT NULL,
  PRIMARY KEY  (USER_ID,BOOK_ID),
  KEY BOOK_ID (BOOK_ID),
  KEY USER_ID (USER_ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- 
-- Volcar la base de datos para la tabla `userfavoritebook`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `userfavoritetab`
-- 

CREATE TABLE USER_FAVORITE_TAB (
  USER_ID int(11) NOT NULL,
  TAB_ID int(11) NOT NULL,
  PRIMARY KEY  (USER_ID,TAB_ID),
  KEY TAB_ID (TAB_ID),
  KEY USER_ID (USER_ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;



CREATE  TABLE REMEMBERME (
  USER_ID INT NOT NULL ,
  REMEMBERME_TOKEN VARCHAR(200) NOT NULL ,
  PRIMARY KEY (USER_ID, REMEMBERME_TOKEN) 
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
