var eventElement;
var callbacks;
$(function(){
	callbacks = $.Callbacks();
	
	addOrEditArtists();
        addOrEditAlbums();
        addArtistButton();
        addAlbumButton();
	$('#prevDiv').modal({backdrop:true, show:false});
//     $('.confirmationYes').click(function() {
//    	
//    	 callbacks.fire(eventElement);
//    	 callbacks.empty();
//    	 $.unblockUI();
//     }); 
//
//     $('.confirmationNo').click(function() { 
//    	 
//    	 callbacks.empty();
//         $.unblockUI(); 
//         return false; 
//     }); 
	
        $('[rel="popover"]').popover();
       
	
});

function addArtistButton(){
    $('#addArtistButton').click(function() {
        var artistName = $('#addArtistAutocomplete').val();
        $("#selectedArtists").append('<span style="font-style:italic;" id="artist_new_'+artistName.hashCode()+'"> '+artistName+' <a href="javascript:removeArtist(\'new_'+artistName.hashCode()+'\');">(x)</a>,</span>');
        $('#selectedArtistsWrap').append('<input type="hidden" id="'+artistName.hashCode()+'" name="artist_new_names[]" value="'+artistName+'" />');
        $("#addArtistAutocomplete").val('');
        if($('#selectedArtistsWrap').is(':hidden'))
        {
            $('#selectedArtistsWrap').slideToggle('slow');
        }
        $('#noArtistFoundMessage').slideToggle();       
    });
}
function addAlbumButton(){
     $('#addAlbumButton').click(function() {
        var albumName = $('#addAlbumAutocomplete').val();
        $("#selectedAlbums").append('<span style="font-style:italic;" id="album_new_'+albumName.hashCode()+'"> '+albumName+' <a href="javascript:removeAlbum(\'new_'+albumName.hashCode()+'\');">(x)</a>,</span>');
        $('#selectedAlbumsWrap').append('<input type="hidden" id="'+albumName.hashCode()+'" name="album_new_names[]" value="'+albumName+'" />');
        $("#addAlbumAutocomplete").val('');
        if($('#selectedAlbumsWrap').is(':hidden'))
        {
            $('#selectedAlbumsWrap').slideToggle('slow');
        }
        $('#noAlbumFoundMessage').slideToggle();  
     });
}

function loadPrev()
{
    if($.trim($('#songTitle').val())==''){
        $('#errorsDiv').modal('show');
        return;
    }
    var wiki_url = baseUrl;
    var url = wiki_url+'/tab/prev';
    var request = $.ajax({
        url: url,
        type: "POST",
        data: $('#tabForm').serialize(),
        dataType: "html"
    }).done(function ( data ) {
        $("#prevTab").html( data );
        $('#prevDiv').modal('show');
  });
 
}    

function saveTab(){
    $('#tabForm').submit();
}

function addOrEditArtists()
{

        var wiki_url = baseUrl;
	var url = wiki_url + 'artist/autocomplete';

	$("#addArtistAutocomplete").autocomplete({
            source: function (request, response) {
            $.ajax({
                url: url,
                dataType: "json",
                data: request,
                success: function (data) {
                    if (!data.length) { // expect [] or ""
                    	$("#addArtistAutocomplete").removeClass('loading');
                    	if(!$('#noArtistFoundMessage').is(':visible'))
                    	{
                                $('[rel="popover"]').popover('hide');
                    		$('#noArtistFoundMessage').slideToggle();
                    	}
                    	response(data);
                    } else {
                    	if($('#noArtistFoundMessage').is(':visible'))
                    	{
                             $('[rel="popover"]').popover('hide');
                    		$('#noArtistFoundMessage').slideToggle();
                    	}
                        response(data);
                    }
                }
            });
        },
	    minLength: 2,
	    select: function (event, ui) {
	    	event.preventDefault();
	    	var hashCode =ui.item.value.hashCode();
	    	if($('#artist_'+hashCode).length == 0)
	    	{
	    		$("#selectedArtists").append('<span style="font-style:italic;" id="artist_'+hashCode+'"> '+ui.item.label+' <a href="javascript:removeArtist(\''+hashCode+'\');">(x)</a>,</span>');
	    		//Add to hidden ID list
	    		$('#selectedArtistsWrap').append('<input type="hidden" id="'+hashCode+'" name="artistId[]" value="'+ui.item.value+'" />');
	    	}
                $("#addArtistAutocomplete").val('');
		if($('#selectedArtistsWrap').is(':hidden'))
	    	{
	    		$('#selectedArtistsWrap').slideToggle('slow');
	    	}
	    },
	    search: function( ) {
			$("#addArtistAutocomplete").addClass('loading');
		},
		open: function( ) {
			
			$("#addArtistAutocomplete").removeClass('loading');
		},
		close: function( ) {
			
			$("#addArtistAutocomplete").removeClass('loading');
		},
		focus: function( event, ui ) {
			$( "#addArtistAutocomplete" ).val( ui.item.label );
			return false;
		}
	});


}


function removeArtist(idArtist){
  
  $('#artist_'+idArtist).remove();
	$('#'+idArtist).remove();
	if($('#selectedArtists').html().trim()=='')
	{
            $('#selectedArtistsWrap').slideToggle('slow');
	}
   
}

function addOrEditAlbums()
{

        var wiki_url = baseUrl;
	var url = wiki_url + 'album/autocomplete';

	$("#addAlbumAutocomplete").autocomplete({
            source: function (request, response) {
            $.ajax({
                url: url,
                dataType: "json",
                data: request,
                success: function (data) {
                    if (!data.length) { // expect [] or ""
                    	$("#addAlbumAutocomplete").removeClass('loading');
                    	if(!$('#noAlbumFoundMessage').is(':visible'))
                    	{
                             $('[rel="popover"]').popover('hide');
                    		$('#noAlbumFoundMessage').slideToggle();
                    	}
                    	response(data);
                    } else {
                    	if($('#noAlbumFoundMessage').is(':visible'))
                    	{
                             $('[rel="popover"]').popover('hide');
                    		$('#noAlbumFoundMessage').slideToggle();
                    	}
                        response(data);
                    }
                }
            });
        },
	    minLength: 2,
	    select: function (event, ui) {
	    	event.preventDefault();
	    	var hashCode =ui.item.value.hashCode();
	    	if($('#album_'+hashCode).length == 0)
	    	{
	    		$("#selectedAlbums").append('<span style="font-style:italic;" id="album_'+hashCode+'"> '+ui.item.label+' <a href="javascript:removeAlbum(\''+hashCode+'\');">(x)</a>,</span>');
	    		//Add to hidden ID list
	    		$('#selectedAlbumsWrap').append('<input type="hidden" id="'+hashCode+'" name="albumId[]" value="'+ui.item.value+'" />');
	    	}
			$("#addAlbumAutocomplete").val('');
			if($('#selectedAlbumsWrap').is(':hidden'))
	    	{
	    		$('#selectedAlbumsWrap').slideToggle('slow');
	    	}
	    },
	    search: function( ) {
			$("#addAlbumAutocomplete").addClass('loading');
		},
		open: function( ) {
			
			$("#addAlbumAutocomplete").removeClass('loading');
		},
		close: function( ) {
			
			$("#addAlbumAutocomplete").removeClass('loading');
		},
		focus: function( event, ui ) {
			$( "#addAlbumAutocomplete" ).val( ui.item.label );
			return false;
		}
	});


}


function removeAlbum(idAlbum){
  
  $('#album_'+idAlbum).remove();
	$('#'+idAlbum).remove();
	if($('#selectedAlbums').html().trim()=='')
	{
            $('#selectedAlbumsWrap').slideToggle('slow');
	}
   
}

String.prototype.hashCode = function(){
    var hash = 0, i, char;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
};