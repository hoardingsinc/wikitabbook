var eventElement;
var callbacks;
$(function(){
	callbacks = $.Callbacks();
	addHoverChords();
        addToFavs();
        changeNotationButton();
        upToneButton();
        downToneButton();
        $('#message').modal({backdrop:false, show:false});
        
         $('#tabLink').click(function (e) {
              
               $('#tabLink').tab('show');
                
               
            })
        $('#tabCommentLink').click(function (e) {
                var wiki_url = baseUrl;
                var url = wiki_url+'/comment/show/'+$('#tabId').val();
                e.preventDefault();
                var request = $.ajax({
                    url: url,
                    type: "GET",
                    data: { tabId: $('#tabId').val()},
                    dataType: "html"
                    }).done(function ( data ) {
                        $('#tabComment').html( data );
                        $('#tabCommentLink').tab('show');
                });
               
            })
        
            $('#tabEditTabLink').click(function (e) {
                
                var wiki_url = baseUrl;
                var url = wiki_url+'/tab/editForm/'+$('#tabRevId').val();
                e.preventDefault();
                var request = $.ajax({
                    url: url,
                    type: "GET",
                    data: { tabRevId: $('#tabRevId').val()},
                    dataType: "html"
                    }).done(function ( data ) {
                        $('#tabEditTab').html( data );
                        $('#tabEditTabLink').tab('show');
                        addOrEditArtists();
                        addOrEditAlbums();
                        addArtistButton();
                        addAlbumButton();
                        $('#prevDiv').modal({backdrop:true, show:false});
                });
               
            })
            
            
            $('#tabEditCommentLink').click(function (e) {
                var wiki_url = baseUrl;
                var url = wiki_url+'/comment/editForm';
                e.preventDefault();
                var request = $.ajax({
                    url: url,
                    type: "GET",
                    data: { tabId: $('#tabId').val()},
                    dataType: "html"
                    }).done(function ( data ) {
                        $('#tabEditComment').html( data );
                        $('#tabEditCommentLink').tab('show');
                      


                });
               
            })
            
            
             $('#tabHistoryTabLink').click(function (e) {
                var wiki_url = baseUrl;
                var url = wiki_url+'/tab/history/'+$('#tabId').val();
                e.preventDefault();
                var request = $.ajax({
                    url: url,
                    type: "GET",
                    data: { tabId: $('#tabId').val()},
                    dataType: "html"
                    }).done(function ( data ) {
                        $('#tabHistoryTab').html( data );
                        $('#tabHistoryTabLink').tab('show');
                });
               
            })
            
});

function addHoverChords(){
    $('.chord').hover(
        function () {
         
            // .position() uses position relative to the offset parent, 
        var pos = $(this).position();

        // .outerWidth() takes into account border and padding.
        var width = $(this).outerWidth();

        //show the menu directly over the placeholder
        var chordName = $(this).text();
        chordName = chordName.replace('#', '\\\\#');
        $('#'+chordName).css({
            position: "absolute",
            top: pos.top + "px",
            left: (pos.left + width) + "px"
        }).fadeIn();
    },  function (){
        var chordName = $(this).text();
        chordName = chordName.replace('#', '\\\\#');
            $('#'+chordName).fadeOut();
        }
    );
}

function changeNotationButton(){
     $('#changeNotationButton').click(function() {
      changeNotation();
     });

}

function upToneButton(){
    $('#upToneButton').click(function(){
       transportTab(1); 
    });
}

function downToneButton(){
    $('#downToneButton').click(function(){
       transportTab(-1); 
    });
}

function addToFavs(){
     $('#addToFavsButton').click(function() {
       var wiki_url = baseUrl;
       var url = wiki_url+'/user/addToFav';
       var request = $.ajax({
        url: url,
        type: "POST",
        data: { tabId: $('#tabId').val()},
        dataType: "json"
        }).done(function ( data ) {
            $("#messageText").html( data.message );
            $('#message').modal('show');
      });
     });
}
