function transportTab(numberOfSemitones) {
	var actualNotation = $('#notation');
	var chords = $('.chord');
	
	if(actualNotation.val() == "spanish")
	{
	chords.each(function (){
			var chordsToChange = $(this).html().split('/');
			var finalChord = "";
			var chordType = "#";
			for(i=0;i<chordsToChange.length;i++)
			{
				if(i>0)
					finalChord+='/';
				var simpleChordName = chordsToChange[i];
				simpleChordName = simpleChordName;
				
				var index = simpleChordName.search(/#/);
				
				if(index!=-1)
				{
					chordType = "#"
				}
				else 
				{
					index = simpleChordName.search(/b/);
					if(index!=-1)
					{
					chordType = "b";	
					}
					else if(simpleChordName.search(/^SOL/)!=-1)
					{
						index=2;
					}
					else
					{
						index=1;
					}
				}
				index = index+1;
				var chordToTransport = simpleChordName.substring(0,index);
				
				var newChord = transportChordSpanish(chordToTransport,numberOfSemitones,chordType);
	
				if(newChord!=-1)
				{
					finalChord+=newChord+simpleChordName.substring(index);
				}	
				
			}	
			$(this).html(finalChord);
		});
	}
	else
	{
		chords.each(function (){
			var chordsToChange = $(this).html().split('/');
			var finalChord = "";
			var chordType = "#";
			for(i=0;i<chordsToChange.length;i++)
			{
				if(i>0)
					finalChord+='/';
				var simpleChordName = chordsToChange[i];
				simpleChordName = simpleChordName;
				
				var index = simpleChordName.search(/#/);
				if(index!=-1)
				{
					chordType = "#"
				}
				else 
				{
					index = simpleChordName.search(/b/);
					if(index!=-1)
					{
					chordType = "b";	
					}
					else
					{
						index=0;
					}
				}
				index = index+1;
				var chordToTransport = simpleChordName.substring(0,index);
				
				var newChord = transportChordEnglish(chordToTransport,numberOfSemitones,chordType);
	
				if(newChord!=-1)
				{
					finalChord+=newChord+simpleChordName.substring(index);
				}	
				
			}	
			$(this).html(finalChord);
		});
	}
  }
  
  function transportChord(originalChord,numberOfSemitones,chords) {
    var originalChordIndex = -1;
    for (i = 0; i < chords.length; i++) {
      if (chords[i] == originalChord) {
        originalChordIndex = i;
        break;
      }
    }
    if (originalChordIndex == -1) {
      alert("Se encontró un error al cambiar de notación el acorde "+originalChord);
      return originalChordIndex;
    }
    var newChordIndex = originalChordIndex + Number(numberOfSemitones);
    while (newChordIndex < 0)
      newChordIndex = chords.length + newChordIndex;
    newChordIndex = newChordIndex % chords.length;
    return chords[newChordIndex];  
  }
  
  function transportChordEnglish(originalChord,numberOfSemitones,chordType)
  {

    if(chordType=="#")
    {
    	return  transportChord(originalChord,numberOfSemitones,
    			new Array("A", "A#", "B", "C", "C#", "D", "D#", "E", 
                        "F", "F#", "G", "G#")); 
    }
    else
    {
  		return transportChord(originalChord,numberOfSemitones,
  				new Array("A", "Bb", "B", "C", "Db", "D", "Eb", "E", 
                        "F", "Gb", "G", "Ab"));       	
    }
  
  }
  
   function transportChordSpanish(originalChord,numberOfSemitones,chordType)
  {
                    
    if(chordType=="#")
    {
    	return  transportChord(originalChord,numberOfSemitones,
    			new Array("LA", "LA#", "SI", "DO", "DO#", "RE", "RE#", "MI", 
                        "FA", "FA#", "SOL", "SOL#")); 
    }
    else
    {
  		return transportChord(originalChord,numberOfSemitones,
  				new Array("LA", "SIb", "SI", "DO", "REb", "RE", "MIb", "MI", 
                        "FA", "SOLb", "SOL", "LAb"));       	
    }
  }
