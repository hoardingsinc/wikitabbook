<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('userBO');
        $this->load->model('remembermeBO');
        $this->load->helper(array('form', 'html'));
    }

    public function login() {

        $userLogged = $this->userBO->login();

        if (count($userLogged) == 0) {
            //Añadir error para mostrar
            $login_error = 'Usuario o password incorrectos.';
            $this->session->set_userdata('login_error', $login_error);
            $this->load->helper('url');
            header('Location:' . site_url("register/loginForm"));
        } else {
            $rememberme = $this->input->post('rememberme');
            if ($rememberme) {
                setcookie('wikiusername', $userLogged[UserBO::USERNAME], time() + 31536000, "/");
                setcookie('wikiuserid', $userLogged[UserBO::ID], time() + 31536000, "/");
                $token = md5(uniqid(rand(), true));
                setcookie('wikitoken', $token, time() + 31536000, "/");
                $this->remembermeBO->save($userLogged[UserBO::ID], $token);
            }
            $this->session->set_userdata('username', $userLogged[UserBO::USERNAME]);
            $this->session->set_userdata('userId', $userLogged[UserBO::ID]);
            $this->load->helper('url');
            $pageFrom = $this->session->userdata('pageAfterLogin');
            if ($pageFrom) {
                $pageFrom = $this->session->userdata('pageAfterLogin');
                $this->session->set_userdata('pageAfterLogin', null);
                header('Location:' . site_url($pageFrom));
            } else {
                header('Location:' . site_url("home"));
            }
        }
    }

    public function logout() {

        if(isset($_COOKIE['wikitoken']))
        {
             $this->remembermeBO->delete($_COOKIE['wikitoken']);
            setcookie('wikiusername',"",time()-3600);
            setcookie('wikiuserid',"",time()-3600);
            setcookie('wikitoken',"",time()-3600);
        }
        $this->session->set_userdata('username', null);
        $this->session->set_userdata('userId', null);
       
        header('Location:' . site_url("home"));
    }

    public function profile() {

        $user = $this->userBO->getOnlyOneByConditions(array(UserBO::ID => $this->session->userdata('userId')));
        $this->setRegisterDate($user);

        $data['user'] = $user;
        $data['collaborations'] = $this->userBO->getColabs($user[UserBO::ID]);
        $data['favs'] = $this->userBO->getFavs($user[UserBO::ID]);
        $this->load->view('user/profile', $data);
    }

    public function editProfileForm() {
        $user = $this->userBO->getOnlyOneByConditions(array(UserBO::ID => $this->session->userdata('userId')));
        $data['username'] = $user[UserBO::USERNAME];
        $this->load->view('user/editProfileForm', $data);
    }

    public function editProfile() {
        
    }

    public function publicProfile($userName) {

        $user = $this->userBO->getByUsername($userName);
        if ($user == null) {
            //Throw error, user doesn't exits
            return;
        } else {
            $this->setRegisterDate($user);
            $data['user'] = $user;

            $data['collaborations'] = $this->userBO->getColabs($user[UserBO::ID]);
            $data['favs'] = $this->userBO->getFavs($user[UserBO::ID]);
            $this->load->view('user/publicProfile', $data);
        }
    }

    private function setRegisterDate($user) {
        if ($user['REGISTER_DATE'] == '' || $user['REGISTER_DATE'] == null) {
            $user['REGISTER_DATE'] = 'antes de WikiTabBook 2.0';
        } else {
            $user['REGISTER_DATE'] = date("d/M/Y", strtotime($user['REGISTER_DATE']));
        }
    }

    public function addToFav() {
        $tabId = $this->input->post('tabId');
        $userId = $this->session->userdata('userId');
        if ($this->userBO->isFav($userId, $tabId)) {
            $this->userBO->removeFav($userId, $tabId);
            $message = array('message' => 'Tab eliminado de favoritos');
            echo json_encode($message);
        } else {
            $this->userBO->addFav($userId, $tabId);
            $message = array('message' => 'Tab agregado a favoritos');
            echo json_encode($message);
        }
    }

}
