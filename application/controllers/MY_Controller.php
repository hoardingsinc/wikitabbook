<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Controller
 *
 * @author david
 */
class MY_Controller {

    //put your code here
    public function __construct() {
        $this->load->model('remembermeBO');
        if (get_cookie('wikitoken') != null) {

            $rememberme = $this->remembermeBO->loadByToken(get_cookie('wikitoken'));
            if ($rememberme != null && sizeof($rememberme) > 0) {
                $this->session->set_userdata('username', get_cookie('wikiusername'));
                $this->session->set_userdata('userId', get_cookie('wikiuserid'));
            }
        }
    }

}

?>
