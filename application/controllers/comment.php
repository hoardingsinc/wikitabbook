<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comment extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('songBO');
        $this->load->model('tabBO');
        $this->load->model('tabRevBO');
        $this->load->model('tabRevArtistBO');
        $this->load->model('tabRevAlbumBO');
        $this->load->model('commentRevBO');
        $this->load->helper(array('form', 'html'));
    }

    public function editForm() {
        $this->load->view('comment/form', array('onlyBody' => TRUE));
    }

    public function create() {

        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<span class="help-inline error">', '</span>');
        if ($this->form_validation->run() == FALSE) {

            $this->load->view('tab/form');
            return;
        }

        $this->db->trans_start();
        $artists = $this->input->post('artistId');
        $albums = $this->input->post('albumId');
        $newArtists = $this->input->post('artist_new_names');
        $newAlbums = $this->input->post('album_new_names');

        if ($newAlbums != false) {
            addAlbums($newAlbums, $albums);
        }
        if ($newArtists != false) {
            addArtists($newArtists, $artists);
        }
        $this->songBO->title = $this->input->post('songTitle');
        $this->tabBO->song_id = $this->songBO->insert();
        $this->tabRevBO->tab_id = $this->tabBO->insert();

        $this->tabRevBO->user_id = intval($this->session->userdata('userId'));
        $this->tabRevBO->commit_comment = '';
        $this->tabRevBO->date = time();
        $this->tabRevBO->notation = $this->input->post('tabNotation');
        $this->tabRevBO->tab = $this->input->post('tab');
        $idTabRev = $this->tabRevBO->insert();
        $this->tabRevArtistBO->tab_rev_id = $idTabRev;
        $this->tabRevAlbumBO->tab_rev_id = $idTabRev;
        if (is_array($artists)) {
            foreach ($artists as $artist) {
                $this->tabRevArtistBO->artist_id = intval($artist);
                $this->tabRevArtistBO->insert();
            }
        } else {
            $this->tabRevArtistBO->artist_id = intval($artists);
            $this->tabRevArtistBO->insert();
        }

        if (is_array($artists)) {
            foreach ($albums as $album) {
                $this->tabRevAlbumBO->album_id = intval($album);
                $this->tabRevAlbumBO->insert();
            }
        } else {
            $this->tabRevAlbumBO->album_id = intval($albums);
            $this->tabRevAlbumBO->insert();
        }
        $this->db->trans_complete();
        $this->load->helper('url');
        header('Location:' . site_url("tab/show/" . urlencode($this->input->post('songTitle')) . "/" . $this->tabRevBO->tab_id));
    }

    function show($tabId) {


        $comment = $this->commentRevBO->loadLastRevision($tabId);
        if (isset($comment['COMMENT'])) {
            $data['comment'] = $comment['COMMENT'];
        } else {
            $data['comment'] = 'Esta canción no tiene comentario sobre cómo tocarla.';
        }
        $data['onlyBody'] = TRUE;
        $this->load->view('comment/show', $data);
    }

}
