<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Album extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('albumBO');
        $this->load->helper(array('form', 'html'));
    }

    public function autocomplete() {
        $toFind = $_GET['term'];
        $result = $this->albumBO->autocomplete($toFind);
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

}

?>