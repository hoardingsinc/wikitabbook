<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Artist extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('artistBO');
        $this->load->helper(array('form', 'html'));
    }

    public function autocomplete() {
        $toFind = $_GET['term'];
        $result = $this->artistBO->autocomplete($toFind);
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function ajaxFind() {

        $toFind = $this->input->get_post('artistName');
        $data['artists'] = $this->artistBO->autocomplete($toFind);

        $this->load->view('ajaxFindArtistsResult', $data);
    }

    public function show($name, $id) {
        $basicArtist = $this->artistBO->getBasicInfo($id);
        $fullArtist = $this->artistBO->getFullById($id);
        $data['fullArtist'] = $fullArtist;
        $data['artistName'] = urldecode($name);
        $data['image'] = $basicArtist['IMAGE_LAST_FM'];
        $data['bio'] = $basicArtist['BIO_LAST_FM'];

        $this->load->view('artist/show', $data);
    }
    
      public function showWithoutId($name) {
        $basicArtist = $this->artistBO->getBasicInfoByName($this->encodeParam($name));
        if($basicArtist==null || sizeof($basicArtist)==0)
        {
            $data['error_msg']='No podemos encontrar este artista en WikiTabBook. ¿Has probado con la búsqueda?';
            $this->load->view('error',$data);
            return;
        }
        $fullArtist = $this->artistBO->getFullById($basicArtist[ArtistBO::ID]);
        $data['fullArtist'] = $fullArtist;
        $data['artistName'] = urldecode($name);
        $data['image'] = $basicArtist['IMAGE_LAST_FM'];
        $data['bio'] = $basicArtist['BIO_LAST_FM'];

        $this->load->view('artist/show', $data);
    }
    
    
    private function encodeParam($param)
	{
		$encodeParam = urldecode($param);
                $encodeParam = str_replace('&#40;', '(', $encodeParam);
                $encodeParam = str_replace('&#41;', ')', $encodeParam);
		return $encodeParam;
	}
}

?>