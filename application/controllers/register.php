<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('userBO');
        $this->load->helper(array('form', 'html'));
    }

    public function form() {
        $data = array();
        $data['captcha'] = $this->createCaptcha();
        $this->load->view('register/form', $data);
    }

    public function loginForm() {
        $this->load->view('register/loginForm');
    }

    public function signup() {
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<span class="help-inline error">', '</span>');
        if ($this->form_validation->run() == FALSE) {
            $data = array();
            $data['captcha'] = $this->createCaptcha();
            $this->load->view('register/form', $data);
        } else {
            $this->userBO->insert();
            $this->load->helper('url');
            header('Location:' . site_url("register/success/"));
        }
    }

    public function success() {
        $data['message_success'] = 'Ya estás registrado. A partir de ahora puedes hacer login con tu usuario y password para colaborar en WikiTabBook.';
        $this->load->view('register/success', $data);
    }

    public function captchaCheck($str) {
        if ($str != $this->session->flashdata('captchaWord')) {
            $this->form_validation->set_message('captchaCheck', 'El captcha no es correcto.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    private function createCaptcha() {
        $this->load->helper('captcha');
        $vals = array(
            'img_path' => './resources/captcha/',
            'img_url' => $this->config->item('base_url') . 'resources/captcha/'
        );

        $cap = create_captcha($vals);

        $this->session->set_flashdata('captchaWord', $cap['word']);

        return $cap['image'];
    }

    public function lostpassword() {

        $this->load->view('register/lostPassword');
    }

    public function sendRandomPassword() {

        $email = $this->input->post('email');
        $user = $this->userBO->getByEmail($email);

        if ($user != null) {
            log_message('debug', 'Encontrado el user: ' . $user[UserBO::USERNAME] . ' para enviarle el correo de actualización de pass');
            $characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            $randomLink = "";
            for ($i = 0; $i < 16; $i++) {
                $randomLink .= substr($characters, rand(0, 62), 1);
            }

            //Insertar link en BBDD

            $updateUser[UserBO::UPDATE_PASS_LINK] = $randomLink;
            $this->userBO->update($user[UserBO::ID], $updateUser);
            $emailBody = '¡Ey ' . $user[UserBO::USERNAME] . "!\r\n\r\n";
            $emailBody .= 'Nos has avisado de que no recuerdas tu password. Pulsa en el siguiente enlace para indicarnos una nueva: ' . site_url('register/updatePassForm/' . $randomLink) . "\r\n";
            $emailBody .= 'Un saludo' . "\r\n\r\n";
            $emailBody .= 'WikiTabBook';
            // In case any of our lines are larger than 70 characters, we should use wordwrap()
            $emailBody = wordwrap($emailBody, 70, "\r\n");
            ini_set('SMTP', 'mail.wikitabbook.com');
            if (!mail($email, 'WikiTabBook - Nueva password', $emailBody, 'From: admin@wikitabbook.com')) {
                //Añadir error de problema al enviar email
                $data['error_msg'] = 'Ha ocurrido un error al enviar el email. Por favor, inténtalo de nuevo en unos minutos.';
                $this->load->view('register/lostPassword', $data);
                return;
            }
            $data['success_msg'] = 'Te hemos enviado un email para actualizar tu password.';
            $this->load->view('register/success', $data);
        } else {
            log_message('debug', 'No se encuentra el email: ' . $email . ' para enviarle el correo de actualización de pass');
            $data['error_msg'] = 'El email que nos indicas no está dado de alta en nuestro sistema.';
            $this->load->view('register/lostPassword', $data);
        }
    }

    public function updatePassForm($updatePassToken) {
        log_message('debug', 'Entramos a actualizar una PASS olvidada con el token: ' . $updatePassToken);
        $user = $this->userBO->getOnlyOneByConditions(array(UserBO::UPDATE_PASS_LINK => $updatePassToken));
        if ($user != null) {
            log_message('debug', 'Usuario encontrado.');
            $this->session->set_userdata('userId', $user[UserBO::ID]);
            $data['name'] = $user[UserBO::USERNAME];
            $this->load->view('register/updatePass', $data);
        } else {
            log_message('warn', 'Error al intentar actualizar una pass olvidada. BAD LINK: ' . $updatePassToken);
            $data['error_msg'] = 'Enlace incorrecto.';
            $this->load->view('errors/error_wiki', $data);
        }
    }

    public function updatePass() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<span class="help-inline error">', '</span>');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('register/updatePass');
        } else {
            //Actualizamos pass y deshabilitamos enlace para update pass
            $this->userBO->update($this->session->userdata('userId'), array(UserBO::PASS => md5($this->input->post('pass')), UserBO::UPDATE_PASS_LINK => ''));
            $data['success_msg'] = 'Tu password ha sido actualizada corréctamente.';
            $this->load->view('register/success', $data);
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */