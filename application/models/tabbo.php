<?php

class TabBO extends CI_Model {

    var $song_id;
    var $id;

    const TABLE = 'TAB';
    const SONG_ID = 'SONG_ID';
    const ID = 'ID';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function insert() {

        $this->db->insert(self::TABLE, $this);

        return $this->db->insert_id();
    }

    function count() {
        return $this->db->count_all(TabBO::TABLE);
    }
    
  

}

?>
