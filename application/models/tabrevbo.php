<?php

class TabRevBO extends CI_Model {

    var $id;
    var $tab_id;
    var $user_id;
    var $notation;
    var $date;
    var $commit_comment='';
    var $tab;

    const TABLE = 'TAB_REV';
    const ID = 'ID';
    const TAB_ID = 'TAB_ID';
    const USER_ID = 'USER_ID';
    const NOTATION = 'NOTATION';
    const DATE = 'DATE';
    const COMMIT_COMMENT = 'COMMIT_COMMENT';
    const TAB = 'TAB';
    const TIME = 'TIME';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function insert() {

        $this->db->insert(self::TABLE, $this);
        return $this->db->insert_id();
    }

    function load() {
        $sqlQuery = 'select TAB_REV.ID as ID, TAB.ID as TAB_ID, TAB_REV.NOTATION as NOTATION, TAB_REV.DATE as DATE, TAB_REV.COMMIT_COMMENT AS COMMIT_COMMENT, TAB_REV.TAB as TAB,SONG.TITLE as TITLE 
						from TAB_REV,TAB inner join SONG on TAB.SONG_ID = SONG.ID
		 					where TAB_REV.ID=' . $this->id . '
							and TAB.ID=TAB_REV.TAB_ID
							 order by TAB_REV.ID desc limit 1';
        $result = $this->db->query($sqlQuery);
        if ($result->num_rows() != 1) {
            log_message('debug', 'No se ha encontrado el Tab con idTabRev:' + $this->id);
            log_message('debug', 'Resultados retornados=' . $result->num_rows());
        } else {
            return $result->row_array();
        }
    }

    function history() {
        $sqlQuery = 'SELECT USER.USERNAME as USERNAME,
       					USER.ID as USER_ID,
        				TAB_REV.ID as TAB_REV_ID,
        				TAB_REV.TAB as TAB,
        				TAB_REV.TAB_ID as TAB_ID,
         				TAB_REV.DATE as DATE,
         				TAB_REV.COMMIT_COMMENT as COMMIT_COMMENT
         		from USER inner join TAB_REV on TAB_REV.USER_ID=USER.ID
         		where TAB_REV.TAB_ID=' . $this->tab_id . ' order by TAB_REV_ID desc';
        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

    function loadLastRevision() {
        $sqlQuery = 'select TAB_REV.ID as ID, TAB.ID as TAB_ID, TAB_REV.NOTATION as NOTATION, TAB_REV.TAB as TAB,SONG.TITLE as TITLE 
						from TAB_REV,TAB inner join SONG on TAB.SONG_ID = SONG.ID
		 					where TAB_REV.TAB_ID=' . $this->tab_id . '
							and TAB.ID=' . $this->tab_id . '
							 order by TAB_REV.ID desc limit 1';
        log_message('debug', $sqlQuery);
        $query = $this->db->query($sqlQuery);
        return $query->row_array();
    }

    function lastChanges() {
        $sqlQuery = 'select SONG.TITLE as SONG_TITLE, TR.ID as TAB_REV_ID, TR.TAB_ID as TAB_ID, TR.USER_ID as USER_ID, 
            TR.DATE as REVISION_DATE, TR.COMMIT_COMMENT as TAB_REV_COMMENT, USER.USERNAME as USERNAME 
            from TAB_REV TR, USER, SONG, TAB where SONG.ID=TAB.SONG_ID and TR.TAB_ID=TAB.ID 
            and TR.USER_ID=USER.ID order by TAB_REV_ID desc limit 40';

        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

    function getArtists() {
        $sqlQuery = 'select TAB_REV_ARTIST.ARTIST_ID as ARTIST_ID, ARTIST.NAME as ARTIST_NAME from
             TAB_REV_ARTIST, ARTIST where TAB_REV_ARTIST.TAB_REV_ID=' . $this->id . ' and TAB_REV_ARTIST.ARTIST_ID=ARTIST.ID';

        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

    function getAlbums() {
        $sqlQuery = 'select TAB_REV_ALBUM.ALBUM_ID as ALBUM_ID, ALBUM.TITLE as ALBUM_TITLE from
             TAB_REV_ALBUM, ALBUM where TAB_REV_ALBUM.TAB_REV_ID=' . $this->id . ' and TAB_REV_ALBUM.ALBUM_ID=ALBUM.ID';

        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

}

?>
