<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of remembermebo
 *
 * @author david
 */
class RemembermeBO extends CI_Model {

    const TABLE = 'REMEMBERME';
    const USER_ID = 'USER_ID';
    const REMEMBERME_TOKEN = 'REMEMBERME_TOKEN';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function loadByToken($token) {
        $result = $this->db->get(RemembermeBO::TABLE, array(self::REMEMBERME_TOKEN => $token));
        return $result->result_array();
    }

    function save($userId, $remembermeToken) {

        $data = array('USER_ID' => $userId,
            'REMEMBERME_TOKEN' => $remembermeToken);
        $this->db->insert(RemembermeBO::TABLE, $data);
    }

    function delete($token){
        $this->db->where('REMEMBERME_TOKEN', $token);
        $this->db->delete(self::TABLE); 
    }
}

?>
