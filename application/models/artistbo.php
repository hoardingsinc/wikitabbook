<?php

class ArtistBO extends CI_Model {

    var $id;
    var $name;

    const TABLE = 'ARTIST';
    const NAME = 'NAME';
    const ID = 'ID';
    const VIEW_ARTIST = '(select TR.TAB_ID AS TAB_ID,
					TR.ID AS TAB_REV_ID,
					TAB_REV_ARTIST.ARTIST_ID AS ARTIST_ID
					from (TAB t join (TAB_REV TR join TAB_REV_ARTIST
						on((TR.ID = TAB_REV_ARTIST.TAB_REV_ID))))
					where ((TR.TAB_ID = t.ID) 
					and (TR.ID = (select max(TAB_REV.ID)
					AS TAB_REV_ID from TAB_REV
					where (TR.TAB_ID = TAB_REV.TAB_ID))))) TAB_REV_PERFORMERS ';
    const VIEW_ALBUM = '(select TR.TAB_ID AS TAB_ID, 
					TR.ID AS TAB_REV_ID,
					TAB_REV_ALBUM.ALBUM_ID AS ALBUM_ID
					from (TAB t join (TAB_REV TR join TAB_REV_ALBUM
						on((TR.ID = TAB_REV_ALBUM.TAB_REV_ID))))
					where ((TR.TAB_ID = t.ID)
						and (TR.ID = 
						(select max(TAB_REV.ID) AS TAB_REV_ID from TAB_REV 
						where (TR.TAB_ID = TAB_REV.TAB_ID))))) TAB_ALBUM ';

    function __construct() {
        // Call the Model consTRuctor
        parent::__construct();
    }

    function getBasicInfo($id) {
        $result = $this->db->get_where(ArtistBO::TABLE, array('ID' => $id));
        return $result->row_array();
    }
    function getBasicInfoByName($name) {
        $result = $this->db->get_where(ArtistBO::TABLE, array('NAME' => $name));
        return $result->row_array();
    }
    
    
    function getFullById($id) {
        $sqlQuery = 'select SONG.TITLE as SONG_TITLE, 
					TAB.ID as TAB_ID, 
					ALBUM.TITLE as ALBUM_TITLE,
					ALBUM.ID as ALBUM_ID
					from ((((SONG inner join TAB on TAB.SONG_ID=SONG.ID)
			 		inner join ' . ArtistBO::VIEW_ARTIST . ' on TAB_REV_PERFORMERS.TAB_ID=TAB.ID)
			 		inner join ' . ArtistBO::VIEW_ALBUM . ' on TAB_ALBUM.TAB_ID=TAB.ID)
                                        inner join ALBUM on ALBUM.ID = TAB_ALBUM.ALBUM_ID)
			 		 where TAB_REV_PERFORMERS.ARTIST_ID=?  
					 order by ALBUM_TITLE,SONG_TITLE';
        $result = $this->db->query($sqlQuery, $id);
        return $result->result_array();
    }

    function autocomplete($toFind) {
        $sql = 'select NAME as label, ID as value from ARTIST where NAME like ?';

        $result = $this->db->query($sql, '%' . $toFind . '%');

        return $result->result_array();
    }

    function find($toFind) {
        $sql = 'select ARTIST.NAME as NAME,
		count(TAB_REV_PERFORMERS.TAB_ID) as NUM_TABS, 
		ARTIST.ID as ID
		from ARTIST inner join ' . ArtistBO::VIEW_ARTIST . ' on ARTIST.ID = TAB_REV_PERFORMERS.ARTIST_ID
		where ARTIST.NAME like ? group by ARTIST.NAME;';

        $result = $this->db->query($sql, '%' . $toFind . '%');

        return $result->result_array();
    }

    function findExactly($toFind) {
        $sql = 'select ARTIST.NAME as NAME,
		count(TAB_REV_PERFORMERS.TAB_ID) as NUM_TABS, 
		ARTIST.ID as ID
		from ARTIST inner join ' . ArtistBO::VIEW_ARTIST . ' on ARTIST.ID = TAB_REV_PERFORMERS.ARTIST_ID
		where ARTIST.NAME=? group by ARTIST.NAME;';

        $result = $this->db->query($sql, $toFind);

        return $result->result_array();
    }

    function insert() {

        $this->db->insert(self::TABLE, $this);

        return $this->db->insert_ID();
    }

}

?>
