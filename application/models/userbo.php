<?php

class UserBO extends CI_Model {

    var $id;
    var $username;
    var $pass;
    var $email;
    var $admin = '0';
    var $not_register = '0';
    var $banned = '0';
    var $description = '';
    //Added V2.0
    var $register_date;
    var $update_date;
    var $ip = '';
    var $update_pass_link = '';

    const TABLE = 'USER';
    const USERNAME = 'USERNAME';
    const PASS = 'PASS';
    const ID = 'ID';
    const UPDATE_PASS_LINK = "UPDATE_PASS_LINK";
    const UPDATE_DATE = "UPDATE_DATE";

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function insert() {
        $this->username = $this->input->post('name');
        $this->pass = md5($this->input->post('pass'));
        $this->email = $this->input->post('email');
        $this->description = $this->input->post('description');

        $this->db->insert(self::TABLE, $this);
    }

    function update($id, $user) {
        $user[UserBO::UPDATE_DATE] = time();
        $this->db->where('ID', $id);
        $this->db->update('USER', $user);
    }

    function login() {
        log_message('debug', 'LOGIN: user=' . $_POST['name'] . ' - pass=' . $_POST['pass']);
        $query = $this->db->get_where('USER', array('USERNAME' => $_POST['name'], 'PASS' => md5($_POST['pass'])));
        if ($query->num_rows == 0) {
            log_message('debug', 'LOGIN:  ERROR EN EL LOGIN');
        } else {
            log_message('debug', 'LOGIN: OK!');
        }
        return $query->row_array();
    }

    function getByEmail($email) {
        $query = $this->db->get_where('USER', array('EMAIL' => $email));
        return $query->row_array();
    }

    function getByUsername($username) {
        $query = $this->db->get_where('USER', array('USERNAME' => $username));
        return $query->row_array();
    }

    function getColabs($userId) {
        $sqlQuery = 'select SONG.TITLE as SONG_TITLE, TR.ID as TAB_REV_ID, TR.TAB_ID as TAB_ID,
            TR.DATE as REVISION_DATE, TR.COMMIT_COMMENT as TAB_REV_COMMENT
            from TAB_REV TR, SONG, TAB where SONG.ID=TAB.SONG_ID and TR.TAB_ID=TAB.ID 
            and TR.USER_ID=' . $userId . ' order by TAB_REV_ID desc';
        $result = $this->db->query($sqlQuery);
        return $result->result_array();
    }

    function getFavs($userId) {
        $sqlQuery = 'select SONG.TITLE as SONG_TITLE, TAB.ID as TAB_ID
            from SONG, TAB, USER_FAVORITE_TAB where SONG.ID=TAB.SONG_ID
            and USER_FAVORITE_TAB.USER_ID=' . $userId . ' and USER_FAVORITE_TAB.TAB_ID=TAB.ID order by TAB.ID desc';
        $result = $this->db->query($sqlQuery);
        return $result->result_array();
    }

    function getOnlyOneByConditions($params) {
        $query = $this->db->get_where('USER', $params);
        return $query->row_array();
    }

    function getListByConditions($params) {
        $query = $this->db->get_where('USER', $params);
        return $query->result_array();
    }

    function findMoreCollaboratives() {
        $sqlQuery = 'select USER.ID as ID, USER.USERNAME as USERNAME, count(*) as NUM_COLLABORATIONS from USER, TAB_REV where USER.ID=TAB_REV.USER_ID group by USER_ID order by NUM_COLLABORATIONS DESC limit 5';
        $result = $this->db->query($sqlQuery);
        return $result->result_array();
    }

    function removeFav($userId, $tabId) {

        $this->db->delete('USER_FAVORITE_TAB', array('USER_ID' => $userId, 'TAB_ID' => $tabId));
    }

    function addFav($userId, $tabId) {
        $data = array('USER_ID' => $userId, 'TAB_ID' => $tabId);
        $this->db->insert('USER_FAVORITE_TAB', $data);
    }

    function isFav($userId, $tabId) {
        $this->db->from('USER_FAVORITE_TAB');
        $this->db->where(array('USER_ID' => $userId, 'TAB_ID' => $tabId));
        return ($this->db->count_all_results() > 0);
    }

}

?>
