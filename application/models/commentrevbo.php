<?php

class CommentRevBO extends CI_Model {

    var $id;
    var $tab_id;
    var $user_id;
    var $comment;
    var $date;
    var $commit_comment;
    var $tab;

    const TABLE = 'COMMENT_REV';
    const ID = 'ID';
    const TAB_ID = 'TAB_ID';
    const USER_ID = 'USER_ID';
    const DATE = 'DATE';
    const COMMIT_COMMENT = 'COMMIT_COMMENT';
    const COMMENT = 'COMMENT';
    const TIME = 'TIME';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function insert() {

        $this->db->insert(self::TABLE, $this);
        return $this->db->insert_id();
    }

    function load() {
        $result = $this->db->get_where(self::TABLE, array(self::ID => intval($this->id)));
        if ($result->num_rows() != 1) {
            log_message('debug', 'No se ha encontrado el Comment con idCommentRev:' + $this->id);
            log_message('debug', 'Resultados retornados=' . $result->num_rows());
        } else {
            return $result->row_array();
        }
    }

    function loadLastRevision($tabId) {
        $sqlQuery = 'select COMMENT_REV.ID as ID, COMMENT_REV.TAB_ID as TAB_ID, COMMENT_REV.COMMENT as COMMENT
						from COMMENT_REV
		 					where COMMENT_REV.TAB_ID = ?
							 order by COMMENT_REV.ID desc limit 1';

        $query = $this->db->query($sqlQuery, $tabId);
        return $query->row_array();
    }

    function lastChanges() {
        $sqlQuery = 'select SONG.TITLE as SONG_TITLE, TR.ID as TAB_REV_ID, TR.TAB_ID as TAB_ID, TR.USER_ID as USER_ID, 
            TR.DATE as REVISION_DATE, TR.COMMIT_COMMENT as TAB_REV_COMMENT, USER.USERNAME as USERNAME 
            from TAB_REV TR, USER, SONG, TAB where SONG.ID=TAB.SONG_ID and TR.TAB_ID=TAB.ID 
            and TR.USER_ID=USER.ID order by TAB_REV_ID desc limit 40';

        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

    function getArtists() {
        $sqlQuery = 'select TAB_REV_ARTIST.ARTIST_ID as ARTIST_ID, ARITST.NAME as ARTIST_NAME from
             TAB_REV_ARTIST, ARTIST where TAB_REV_ARTIST.TAB_REV_ID=' . $this->id . ' and TAB_REV_ARTIST.ARTIST_ID=ARTIST.ID';

        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

    function getAlbums() {
        $sqlQuery = 'select TAB_REV_ALBUMS.albums_id as albumId, album.title as albumTitle from
             tab_rev_album, album where tab_rev_album.tab_rev_id=' . $this->id . ' and tab_rev_album.album_id=album.id';

        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

}

?>
