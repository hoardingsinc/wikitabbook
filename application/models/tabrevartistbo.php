<?php

class TabRevArtistBO extends CI_Model {

    var $tab_rev_id;
    var $artist_id;

    const TABLE = 'TAB_REV_ARTIST';
    const TAB_REV_ID = 'TAB_REV_ID';
    const ARTIST_ID = 'ARTIST_ID';

    function __construct() {
// Call the Model constructor
        parent::__construct();
    }

    function insert() {

        $this->db->insert(self::TABLE, $this);
    }

}

?>
