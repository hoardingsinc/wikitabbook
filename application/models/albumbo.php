<?php

class AlbumBO extends CI_Model {

    var $id;
    var $title;

    const TABLE = 'ALBUM';
    const TITLE = 'TITLE';
    const ID = 'ID';
    const VIEW_ALBUM = '(select TR.TAB_ID AS TAB_ID, 
					TR_ID AS TAB_REV_ID,
					TAB_REV_ALBUM.ALBUM_ID AS ALBUM_ID
					from (tab T join (TAB_REV TR join TAB_REV_ALBUM
						on((TR.ID = TAB_REV_ALBUM.TAB_REV_ID))))
					where ((TR.TAB_ID = T.ID)
						and (TR.ID = 
						(select max(TAB_REV.ID) AS TAB_REV_ID from TAB_REV
						where (TR.TAB_ID = TAB_REV.TAB_ID))))) TAB_ALBUM ';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function load($id) {
        $result = $this->db->get(AlbumBO::TABLE, array('id' => $id));
        return $result->result_array();
    }

    function autocomplete($toFind) {
        $sql = 'select TITLE as label, ID as value from ALBUM where TITLE like ?';

        $result = $this->db->query($sql, '%' . $toFind . '%');

        return $result->result_array();
    }

    function find($toFind) {
        $sql = 'select ARTIST.NAME as ARTIST_NAME, ALBUM.TITLE as TITLE, 
		ARTIST.ID as ARTIST_ID,ALBUM.ID as ID 			
		from ((ALBUM inner join ARTIST_ALBUM  				
		on ALBUM.ID= ARTIST_ALBUM.ALBUM_ID)      		
		inner join ARTIST  			
			on ARTIST.ID = ARTIST_ALBUM.ARTIST_ID)  where TITLE like ? AND TITLE!=\'Indefinido\' ORDER BY TITLE';

        $result = $this->db->query($sql, '%' . $toFind . '%');

        return $result->result_array();
    }

    function findExactly($toFind) {
        $sql = 'select ARTIST.NAME as ARTIST_NAME, ALBUM.TITLE as TITLE, 
		ARTIST.ID as ARTIST_ID,ALBUM.ID as ID 
		from ((ALBUM inner join ARTIST_ALBUM  				
		on ALBUM.ID= ARTIST_ALBUM.ALBUM_ID)      		
		inner join ARTIST  			
			on ARTIST.ID = ARTIST_ALBUM.ARTIST_ID)  where TITLE=? AND TITLE!=\'Indefinido\' ORDER BY TITLE';

        $result = $this->db->query($sql, $toFind);

        return $result->result_array();
    }

    function insert() {

        $this->db->insert(self::TABLE, $this);

        return $this->db->insert_id();
    }

}

?>
