<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div class="wiki-hero-unit">

    <h3>Ups!</h3>
    <p><?= $error_msg ?></p>
    <p>WikiTabBook Team</p>
</div><!--/span-->
<?php
require ('application/views/tiles/footer.php');
?>   