<?php if (isset($songResults)) { ?>
    <?php if (sizeof($songResults) > 0) { ?>
        <table  class="table table-hover">
            <thead>
                <tr>
                    <th style="width:100px;">Nombre del artista</th>
                    <th style="width:150px;">Título de la canción</th>               
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($songResults as $song) {
                    echo '<tr>';
                    echo '<td>' . $song['ARTIST_NAME'] . '</td>';
                    echo '<td><a href="' . site_url('tab/show/' . urlencode($song['SONG_TITLE'])) . '/' . $song['TAB_ID'] . '#">' . $song['SONG_TITLE'] . '</a></td>';
                    echo '</tr>';
                }
                ?>

            </tbody>
        </table>
    <?php
    } else {
        echo 'No se han encontrado resultados';
    }
    ?>
<?php } else if (isset($artistResults)) { ?>
    <?php if (sizeof($artistResults) > 0) { ?>
        <table  class="table table-hover">
            <thead>
                <tr>
                    <th style="width:100px;">Nombre del artista</th>
                    <th style="width:150px;">Número de Tabs</th>               
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($artistResults as $artist) {
                    echo '<tr>';
                    echo '<td><a href="' . site_url('artist/show/' . urlencode($artist['NAME']) . '/' . $artist['ID']) . '">' . $artist['NAME'] . '</a></td>';
                    echo '<td>' . $artist['NUM_TABS'] . '</td>';
                    echo '</tr>';
                }
                ?>

            </tbody>
        </table>
    <?php
    } else {
        echo 'No se han encontrado resultados';
    }
    ?>
<?php } else if (isset($albumResults)) { ?>
    <?php if (sizeof($albumResults) > 0) { ?>
        <table  class="table table-hover">
            <thead>
                <tr>
                    <th style="width:100px;">Nombre del artista</th>
                    <th style="width:150px;">Título del disco</th>               
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($albumResults as $album) {
                    echo '<tr>';
                    echo '<td><a href="' . site_url('artist/show/' . urlencode($album['ARTIST_NAME']) . '/' . $album['ARTIST_ID']) . '">' . $album['ARTIST_NAME'] . '</a></td>';
                    echo '<td><a href="' . site_url('artist/show/' . urlencode($album['ARTIST_NAME']) . '/' . $album['ARTIST_ID']) . '#' . $album['ID'] . '">' . $album['TITLE'] . '</a></td>';
                    echo '</tr>';
                }
                ?>

            </tbody>
        </table>
    <?php
    } else {
        echo 'No se han encontrado resultados';
    }
    ?>
<?php } ?>

