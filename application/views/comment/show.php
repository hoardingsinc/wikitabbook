<?php
if (!isset($onlyBody)) {
    require ('application/views/tiles/head.php');
    require('application/views/tiles/header.php');
    require('application/views/tiles/menu.php');
}
?>
<div class="fieldset fieldset-comment">

    <h2>Comentario</h2>

    <?= $comment ?>

</div>
<?php
if (!isset($onlyBody)) {
    $customScripts = array('tabShow', 'tabNotation', 'transportTab');

    require ('application/views/tiles/footer.php');
}
?>   
