<?php
if (!isset($onlyBody)) {
    require ('application/views/tiles/head.php');
    require('application/views/tiles/header.php');
    require('application/views/tiles/menu.php');
}
?>
<div class="fieldset fieldset-newTab">
    <form class="form-horizontal" method="post" id="tabForm" action="<?= site_url("tab/create"); ?>">
        <fieldset>
            <legend class="formLegend">Indícanos título, artista, disco, notación y el Tab en ChordPro</legend>
            <div class="control-group">
                <label class="control-label" for="songTitle">Título</label>
                <div class="controls">
                    <input type="text" placeholder="Título de la canción" class="input-xlarge typeahead" id="songTitle" name="songTitle">
                    <?php echo form_error('songTitle'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="tabArtists">¿Quién la toca?</label>
                <div class="controls">
                    <input type="text" rel="popover" data-trigger="focus" data-content="Según vayas introduciendo el nombre del artista lo iremos buscando en WikiTabBook para evitar duplicados. Ah, y puedes añadir varios!" data-title="Tip"
                           id="addArtistAutocomplete" placeholder="Nombre del artista"  name="artistName" class="input-append" id="artistName">
                    <span id="noArtistFoundMessage" style="display:none;"><input type="button" id="addArtistButton" class="btn btn-primary" value="Añadir"></input>
                        Ey! No lo encontramos. Dale a "Añadir" para incluirlo en WikiTabBook</span>
                </div>

            </div>

            <div class="control-group" style="display:none" id="selectedArtistsWrap" >
                <label class="control-label" for="tabArtists">Artistas Seleccionados:</label>
                <div class="controls">
                    <span id="selectedArtists" style="float:left; padding-top:5px;">
                    </span>
                </div>
            </div>	
            <div class="control-group">
                <label class="control-label" for="tabAlbums">¿En qué disco?</label>
                <div class="controls">
                    <input type="text" rel="popover" data-trigger="focus" data-content="Según vayas introduciendo el título lo iremos buscando en WikiTabBook para evitar duplicados. Ah! Y puedes añadir varios!" data-title="Tip" 
                           id="addAlbumAutocomplete" placeholder="Título del disco"  name="albumName" class="input-append" id="albumName">
                    <span id="noAlbumFoundMessage" style="display:none;">
                        <input type="button" id="addAlbumButton" class="btn btn-primary" value="Añadir"></input>
                        Ey! No lo encontramos. Dale a "Añadir" para incluirlo en WikiTabBook</span>
                </div>

            </div>

            <div class="control-group" style="display:none" id="selectedAlbumsWrap" >
                <label class="control-label" for="tabAlbums">Discos Seleccionados:</label>
                <div class="controls">
                    <span id="selectedAlbums" style="float:left; padding-top:5px;">
                    </span>
                </div>
            </div>	

            <div class="control-group">
                <label class="control-label" for="tabNotation">Notación</label>
                <div class="controls">
                    <select class="input-xlarge" id="tabNotation" name="tabNotation">
                        <option value="english">Inglesa</option>
                        <option value="spanish">Española</option>
                    </select>
                    <p class="help-block">Inglesa: A, B, C, D, E, F, G - Española: LA, SI, DO, RE, MI, FA, SOL</p>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="tab">Tab en ChordPro</label>
                <div class="controls">
                    <textarea rows="20" style="width:80%;" id="tab" name="tab">{title:}
{subtitle:}

{start_of_chorus}
{end_of_chorus}

{comment:}</textarea>
                </div>
            </div>
            <div class="text-center">
                <input type="button" onclick="javascript:loadPrev();return false;" class="btn btn-primary" value="Previsualizar y/o Guardar"></input>
            </div>
        </fieldset>
    </form>
</div><!--/span-->
<div id="prevDiv" style="width:40%;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Prev" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Previsualización</button>
        <h3>Previsualización</h3>
    </div>
    <div class="modal-body">
        <div id="prevTab"></div>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="$('#prevDiv').modal('hide')" class="btn btn-primary" value="Volver al editor"></input>
        <input type="button" onclick="javascript:saveTab();return false;" class="btn btn-primary" value="Guardar"></input>
    </div>
</div>
<div id="errorsDiv" style="width:40%;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Error" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Cerrar</button>
        <h3>WikiTabBook</h3>
    </div>
    <div class="modal-body">
        El título es obligatorio!
    </div>
    <div class="modal-footer">
        <input type="button" onclick="$('#errorsDiv').modal('hide')" class="btn btn-primary" value="Volver al editor"></input>
    </div>
</div>
</div>


<?php
$customScripts = array('tabForm');
if (!isset($onlyBody)) {
    require ('application/views/tiles/footer.php');
}
?>   
