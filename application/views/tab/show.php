<?php
if (!isset($onlyBody)) {
    require ('application/views/tiles/head.php');
    require('application/views/tiles/header.php');
    require('application/views/tiles/menu.php');
}
?>
<div class="mainContent">

    <div class="span8">
        <div class="page-header">
            <?php
            $artistsNames = '';
            foreach ($artists as $artist) {
                if ($artistsNames != '') {
                    $artistsNames = $artistsNames . '/';
                }
                $artistsNames = $artistsNames . $artist['artistName'];
            }
            ?>
            <h2><?= $songTitle ?> - <small><?= $artistsNames ?></small></h2>
        </div>

        <div class="btn-group">
            <button type="button" id="addToFavsButton" class="btn btn-primary <? if (isset($isFav)) echo 'active'; ?>" data-toggle="button"><i class="icon-heart"></i> Favorita</button>
            <button type="button" id="changeNotationButton" class="btn btn-primary" data-toggle="button"><i class="icon-retweet"></i> Cambiar notación</button>
            <button type="button" id="upToneButton" class="btn btn-primary"><i class="icon-arrow-up"></i> Subir tono</button>
            <button type="button" id="downToneButton" class="btn btn-primary"><i class="icon-arrow-down"></i> Bajar tono</button>
        </div>
        <div id="shareThis" style="float:right;position: relative; top:50px;">
            <span class='st_facebook_large' displayText='Facebook'></span>
            <span class='st_twitter_large' displayText='Tweet'></span>
            <span class='st_googleplus_large' displayText='Google +'></span>
            <span class='st_pinterest_large' displayText='Pinterest'></span>
            <span class='st_email_large' displayText='Email'></span>
        </div>

        <input type="hidden" id="notation" value="<?= $notation ?>"></input>
        <input type="hidden" id="tabId" value="<?= $tabId ?>"></input>
        <?= $tab ?>


    </div>

    <div id="message" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Prev" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Favoritos</button>
            <h3>WikiTabBook</h3>
        </div>
        <div class="modal-body">
            <div id="messageText"></div>
        </div>
        <div class="modal-footer">
            <input type="button" onclick="$('#message').modal('hide')" class="btn btn-primary" value="Ok"></input>

        </div>
    </div>

</div>
<?php
if (!isset($onlyBody)) {
    $customScripts = array('tabShow', 'tabNotation', 'transportTab');

    require ('application/views/tiles/footer.php');
}
?>   
