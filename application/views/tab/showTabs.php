<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div class="mainContent">

    <div class="span11">
        <div class="page-header" style="margin:-15px 0 -60px;">

            <h2><?= $songTitle ?> - <small><?= $artistsNames ?></small></h2>
        </div>
        <div class="btn-group" style="top:190px;right:10px;text-align: right;">
            <button type="button" id="addToFavsButton" title="Agregar a favoritos" class="btn <? if (isset($isFav)) echo 'active'; ?>" data-toggle="button"><i class="icon-heart"></i></button>
            <button type="button" id="changeNotationButton" title="Cambiar notación Inglesa/Española" class="btn" data-toggle="button"><i class="icon-retweet"></i></button>
            <button type="button" id="upToneButton" title="Subir tono" class="btn"><i class="icon-arrow-up"></i></button>
            <button type="button" id="downToneButton" title="Bajar Tono" class="btn"><i class="icon-arrow-down"></i></button>
        </div>

        <div id="shareThis" style="float:right;position: relative; top:50px;">

            <span class='st_facebook_large' displayText='Facebook'></span>
            <span class='st_twitter_large' displayText='Tweet'></span>
            <span class='st_googleplus_large' displayText='Google +'></span>
            <span class='st_pinterest_large' displayText='Pinterest'></span>
            <span class='st_email_large' displayText='Email'></span>
        </div>

        <input type="hidden" id="notation" value="<?= $notation ?>"></input>
        <input type="hidden" id="tabId" value="<?= $tabId ?>"></input>
        <input type="hidden" id="tabRevId" value="<?= $tabRevId ?>"></input>

        <div class="tabbable">
            <ul id="tabs" class="nav nav-tabs">
                <li class="active"><a id="tabLink" href="#tab">Tab</a></li>
                <li><a id="tabCommentLink" href="#tabComment">Comentario</a></li>
                <?php
                $user = $this->session->userdata('userId');
                if ($user) {
                    ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle"
                           data-toggle="dropdown"
                           href="#">
                            Editar
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a id="tabEditTabLink" href="#tabEditTab" >Tab</a></li>
                            <li><a id="tabEditCommentLink" href="#tabEditComment">Comentario</a></li>
                        </ul>
                    </li>
<?php } ?>
                <li class="dropdown">
                    <a class="dropdown-toggle"
                       data-toggle="dropdown"
                       href="#">
                        Historial
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a id="tabHistoryTabLink" href="#tabHistoryTab" >Tab</a></li>
                    </ul>
                </li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab">
<?= $tab ?>
                </div>
                <div class="tab-pane" id="tabComment">
                    <p>Howdy, I'm in Section 2.</p>
                </div>
                <div class="tab-pane" id="tabEditTab">
                    <p>Ey estas editando el tab</p>
                </div>
                <div class="tab-pane" id="tabEditComment">
                    <p>Ey estas editando el comment</p>
                </div>
                <div class="tab-pane" id="tabHistoryTab">
                    <p>Howdy, I'm in Section 2.</p>
                </div>


            </div>




        </div>

        <div id="message" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Prev" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Favoritos</button>
                <h3>WikiTabBook</h3>
            </div>
            <div class="modal-body">
                <div id="messageText"></div>
            </div>
            <div class="modal-footer">
                <input type="button" onclick="$('#message').modal('hide')" class="btn btn-primary" value="Ok"></input>

            </div>
        </div>

    </div>
</div>
<?php
$customScripts = array('tabShow', 'tabNotation', 'tabForm', 'transportTab');
require ('application/views/tiles/footer.php');
?>   
