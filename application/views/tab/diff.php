<?php
if (!isset($onlyBody)) {
    require ('application/views/tiles/head.php');
    require('application/views/tiles/header.php');
    require('application/views/tiles/menu.php');
}
?>
<div class="fieldset fieldset-historyTab">

    <div class="page-header">
        <h3>Diff</h3>
    </div>
    <table style="width:100%">
        <tbody>
            <tr>
                <td><h5>Revisión: <?= $tabRev2[TabRevBO::DATE] ?>  [<a href="<?= site_url('tab/showRevision/' . $tabRev2['ID']) ?>">Editar</a>]</h5></td>
                <td><h5>Revisión: <?= $tabRev1[TabRevBO::DATE] ?>  [<a href="<?= site_url('tab/showRevision/' . $tabRev1['ID']) ?>">Editar</a>]</h5></td>
            </tr>
        </tbody>

    </table>
    <?= $diff ?>       

</div>


<?php
if (!isset($onlyBody)) {

    require ('application/views/tiles/footer.php');
}
?>   
