<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div id="profile" class="wiki-hero-unit">

    <div class="row">
        <div class="span3"><img src="<?= $image ?>" height="200px" width="200px" class="img-rounded"></div>
        <div class="span6">
            <h3><?= $artistName ?></h3>
            <h5>Número de Tabs: <?= sizeof($fullArtist) ?></h5>

        </div>
    </div>
    <?php
    $currentAlbumId = '';
    foreach ($fullArtist as $artistTab) {
        if ($currentAlbumId != $artistTab['ALBUM_ID']) {
            if ($currentAlbumId != '') {
                echo '</tbody>';
                echo '</table>';
                echo '</div>';
                echo '</div>';
            }
            $currentAlbumId = $artistTab['ALBUM_ID'];
            echo '<div class="row">';
            echo '<div class="span10">';
            echo '<h4><a href="#' . $currentAlbumId . '">' . $artistTab['ALBUM_TITLE'] . '</a></h4>';
            echo '<table  class="table table-hover">';

            echo '<tbody>';
        }
        echo '<tr>';
        echo '<td><a href="' . site_url('tab/show/' . urlencode($artistTab['SONG_TITLE']) . '/' . $artistTab['TAB_ID']) . '">' . $artistTab['SONG_TITLE'] . '</a></td>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
    echo '</div>';
    echo '</div>';
    ?>



</div>  

<?php
require ('application/views/tiles/footer.php');
?>   