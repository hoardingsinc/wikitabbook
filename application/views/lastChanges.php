<?php
require('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div class="row">
    <div class="span11 mainContent">
        <div class="wiki-hero-unit">
            <h3>Últimos cambios</h3>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width:100px;">Fecha Revisión</th>
                        <th style="width:250px;">Canción</th>
                        <th style="width:150px;">Usuario</th>
                        <th>Comentario</th>
                    </tr>
                </thead>
                <tbody

                    <?php
                    foreach ($lastChanges as $change) {
                        echo '<tr>';
                        echo '<td>' . $change['REVISION_DATE'] . '</td>';
                        echo '<td><a href="' . site_url('tab/show/' . urlencode($change['SONG_TITLE'])) . '/' . $change['TAB_ID'] . '#">' . $change['SONG_TITLE'] . '</a></td>';
                        echo '<td>' . $change['USERNAME'] . '</td>';
                        echo '<td>' . $change['TAB_REV_COMMENT'] . '</td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div><!--/span-->

</div>
<?php
$customScripts = array('lastChanges');

require ('application/views/tiles/footer.php');
?>   