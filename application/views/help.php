<?php
require('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?> 

<div class="span11 mainContent">
    <h2>Ayuda</h2>
    <hr />
    <h3>Introducción</h3>

    <p>WikiTabBook es una página para consultar y publicar transcripciones
        de canciones. En este pequeño manual se explica lo básico de <a
            href="#consulta">cómo consultar transcripciones</a> y qué herramientas
        se proporcionan, y <a href="#creacion">cómo crear transcripciones</a>,
        incluyendo una descripción del <a href="#chordpro">formato chrodpro</a>.
        Hay funcionalidades no explicadas aquí y que puedes explorar por ti
        mismo. Si tienes dudas, utiliza <a
            href="http://foro.wikitabbook.com/">el foro</a>.</p>

    <h3><a name="consulta"></a>Cómo consultar transcripciones</h3>

    <p>El uso básico de la página para consultar transcripciones es muy
        sencillo. Para empezar, puedes buscar canciones que te interesen a
        través del campo destinado para ello en el menú de la izquierda. Puedes
        buscar por el nombre de la canción, del artista o del álbum en el que
        aparece. Tras pulsar el botón "Buscar" te aparecerá una lista de los
        elementos encontrados. Pulsando sobre el título de una canción te lleva
        a su transcripción; pulsando sobre el nombre e un artista o de un
        disco, te lleva a las transcripciones que hay de ese artista o
        disco.</p>

    <p>En la página de una transcripción, además de verla, tienes un nuevo
        menú que te permite, entre otras cosas, crear un archivo PDF para
        imprimirla, cambiar la notación entre la inglesa (A, B, C...) y la
        española (LA, SI, DO...) y cambiar el tono de la canción.</p>

    <p>Fíjate que, además de ver los acordes al final de la transcripción,
        si pasas por encima del nombre de alguno, te saldrá una figura indicando
        cómo se toca.</p>

    <p>Esto es un wiki (si no sabes lo que es, lee <a
            href="http://es.wikipedia.org/wiki/Wiki">la descripción que hay en la
            Wikipedia</a>), eso significa que puede haber varias versiones de la
        misma transcripción, a las que puedes acceder desde la pestaña
        "Historial" situada en la zona superior derecha. También puedes ver
        comentarios sobre la transcripción con la pestaña "Comentarios". Una vez
        en la página de comentarios, puedes añadirlo pulsando en la pestaña
        "Editar".</p>

    <h3><a name="creacion"></a>Cómo crear transcripciones</h3>

    <p>En primer lugar, recuerda que no puedes copiar el trabajo de otros.
        <b>Sólo se aceptan transcripciones originales.</b>

    <p>Para crear o modificar transcripciones, debes registrarte siguiendo
        el enlace situado arriba a la izquierda. Una vez registrado, tendrás una
        página personal en la que ver tus contribuciones, poner una pequeña
        descripción sobre ti, cambiar tu contraseña o ver tus transcripciones
        favoritas.</p>

    <p>Para crear una transcripción, debes seguir el enlace "Nuevo tab" del
        menú de la izquierda. Te aparecerá un formulario con varios campos, de
        los cuales sólo es obligatorio el título y, lógicamente, la propia
        transcripción.</p>

    <p>Los campos de título de la transcripción, nombre del artista y álbum
        en el que están incluidos funcionan de una forma a la que tal vez no
        estés acostumbrado. Vamos a explicarlo para nombre del tab, pero los
        otros dos funcionan de manera similar. Para rellenar el nombre del tab,
        tienes que seguir estos pasos:</p>

    <ul>

        <li>Escribe el título en el campo correspondiente. <b>Con eso no te
                vale</b>.
        </li>

        <li>Pulsa "Buscar".
        </li>

        <li>Te puede salir una lista de canciones y entre ellas puede que ya
            esté la que tú quieres. En ese caso, pulsa el signo más que aparece al
            lado de su nombre. Si no encuentras la canción en la lista, pulsa
            sobre "No encuentro la canción, crear uno nueva" y luego pulsa en
            "Agregar".
        </li>

    </ul>

    <p>Al final tienes que tener en la lista de "Seleccionados" la canción
        (o los artistas y discos) que desees. Tras esto, ya puedes rellenar la
        transcripción en el área correspondiente. Para ello debes utilizar el
        formato chordpro que se explica en el apartado siguiente.</p>

    <p>Una última cosa: también puedes añadir comentarios sobre la
        transcripción (si tienes dudas, explicación de decisiones que has tomado
        al transcribir, o detalles de cómo se toca la canción) mediante la
        pestaña "Comentarios" de la parte superior derecha. Después de acceder a
        ella, debes darle a "Editar". Puedes utilizar formato HTML para poner
        los comentarios.</p>

    <h3><a name="chordpro"></a>El formato chordpro</a></h3>

    <p>El formato chordpro es un formato simple que permite hacer
        transcripciones que luego se puedan tratar para que queden
        <i>bonitas</i>. Tiene unas cuantas posibilidades, pero lo único
        fundamental que debes saber es lo que se explica en el siguiente
        párrafo.</p>

    <p>Para crear la transcripción, trabaja con un archivo de texto en tu
        ordenador. Para ello sólo necesitas utilizar un procesador de texto (el
        Bloc de notas que trae Windows vale perfectamente; el Word no es
        recomendable). Para poner la letra de la canción con los acordes encima,
        lo que debes hacer es poner el acorde entre corchetes justo antes de la
        sílaba encima de la cual quieres que aparezca el acorde. Por
        ejemplo:</p>

    <pre>    y san[D]grando los dedos [E] de tanto que lo inten[A]té.
    </pre>

    <p>hará que salga una "D" encima de "gran", una "E" en el espacio entre
        "dedos" y "de" y una A encima de "té".</p>

    <p>El nombre de los acordes lo puedes poner en español (LA, SI, DO...) o
        en inglés (A, B, C...). A la hora de ver la transcripción se puede
        cambiar entre las dos notaciones.</p>

    <p>El formato chordpro tiene unas cuantas directivas que permiten poner
        cosas que no son directamente la letra y los acordes. Son las
        siguientes:</p>

    <ul>

        <li>Poner título a la canción. Basta con poner:
        </li>

    </ul>

    <pre>      {title: Título de la canción que quieras}
    </pre>

    <ul>

        <li>Poner subtítulo a la canción. Basta con poner:
        </li>

    </ul>

    <pre>      {subtitle: Título de la canción que quieras}
    </pre>

    <ul>

        <li>Para marcar el estribillo (sólo sirve para que se vea un poco
            distinto al imprimir):
        </li>

    </ul>

    <pre>      {start_of_chorus}
      Aquí iría el estribillo,
      que rima con amarillo.
      {end_of_chorus}
    </pre>

    <ul>

        <li>Poner una transcripción literal:
        </li>

    </ul>

    <pre>      {sot}
      Esto se copiaría literalmente. Sirve, por ejemplo para poner 
      tablaturas o para indicar acordes que van sin letra, como:

      | A | D | A | E |
      {eot}
    </pre>

    <ul>

        <li>Poner comentarios:
        </li>

    </ul>

    <pre>       {comment: Lo que quieras de comentario.} 
       {comment: Por ejemplo, indicar si se utiliza cejilla}
       {comment: Los comentarios sólo pueden tener una línea}
    </pre>

    <ul>

        <li>Describir un acorde. Por defecto, WikiTabBook tiene la descripción
            de varios acordes, pero puede que en la canción que estés
            transcribiendo falte alguno de los que usas o haya una posición
            diferente a la que tiene WikiTabBook por defecto. En ese caso, puedes
            definir un acorde utilizando la directiva "define". Viendo el
            siguiente ejemplo, que define la posición típica de G (sol mayor) con
            cejilla y la de Aadd9 (la con novena añadida), te debería quedar claro
            cómo funciona: </li>

    </ul>

    <pre>       {define: G base-fret 3 frets 1 3 3 2 1 1} 
       {define: Aadd9 base-fret 0 frets x 0 2 2 0 0}
    </pre>

    <p>Si tienes dudas, además de consultar en <a
            href="http://foro.wikitabbook.com/">el foro</a>, puedes editar otras
        transcripciones para ver cómo definen sus acordes.</p>
</div>

<?php
require ('application/views/tiles/footer.php');
?>  