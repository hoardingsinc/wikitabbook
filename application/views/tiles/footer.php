</div><!--/row-->

<footer class="footer">
    <p style="color:white">&copy; Kaylan 2012</p>
</footer>

</div><!--/.fluid-container-->

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<?php
require ('application/views/tiles/scripts.php');
if (isset($customScripts)) {
    foreach ($customScripts as $script) {
        echo '<script src="' . $this->config->item("base_url") . '/resources/js/' . $script . '.js"></script>';
    }
}
?>
</div>
</body>
</html>
