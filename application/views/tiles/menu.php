
<div class="navbar clear">

    <div class="navbar-inner ">
        <ul class="nav">
            <li><a href="<?= site_url('home') ?>">Home</a></li>
            <li><a href="<?= site_url('home/lastChanges') ?>">Últimos cambios</a></li>
            <li><a href="<?= site_url('tab/form') ?>">Nuevo Tab</a></li>
            <li><a href="http://foro.wikitabbook.com">Foro</a></li>
            <li><a href="<?= site_url('home/help') ?>">Ayuda</a></li>
        </ul>

    </div>
    <form class="form-search" action="<?= site_url('home/search') ?>" method="get">

        <input type="text" name="search" placeholder="Título, artista..." class="input-medium search-query">
        <button type="submit" class="btn btn-primary"><i class="icon-search"></i> Buscar!</button>
    </form>
</div>