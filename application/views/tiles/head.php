<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php
if (isset($pageTitle)) {
    echo $pageTitle;
} else {
    echo 'WikiTabBook - Tabs para guitarra';
}
?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Tabs para guitarra colaborativos.">
        <meta name="author" content="Kaylan!">
        <!-- DataTables CSS -->
        <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">

        <!-- Le styles -->
        <link href="<?= $this->config->item('base_url') ?>resources/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?= $this->config->item('base_url') ?>resources/jquery/css/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet">
        <link href="<?= $this->config->item('base_url') ?>resources/css/wiki.css" rel="stylesheet">
        <link href="<?= $this->config->item('base_url') ?>resources/css/chordpro.css" rel="stylesheet">
        <link href="<?= $this->config->item('base_url') ?>resources/css/tab-print.css" rel="stylesheet"  media="print">
        <style type="text/css">
            body {
                padding-bottom: 10px;
            }
            .sidebar-nav {
                padding: 0;
            }
        </style>
       <!-- <link href="<?= $this->config->item('base_url') ?>/resources/bootstrap/css/bootstrap-responsive.css" rel="stylesheet"> -->

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons 
        <link rel="shortcut icon" href="../assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
        -->

        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript">var baseUrl='<?= $this->config->item('base_url') ?>';</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "5e0b4b18-ff95-49d6-ac73-c470948646ed"});</script>
    </head>