<body>
    <?php include_once("analytics.php") ?>
    <div class="container">
        <div id="header">
            <a href="<?= site_url("home"); ?>" class="logo">WikiTabBook</a>

            <?php if (isset($this->session->userdata['username'])) { ?>
                <div class="pull-right userOptionsButton">
                    <div class="btn-group">
                        <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user"></i>
                            <?= $this->session->userdata['username']; ?> [Opciones]
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= site_url('user/profile'); ?>"><i class="icon-info-sign"></i> Mi Perfil</a></li>

                            <li><a href="<?= site_url('user/logout'); ?>"><i class="icon-off"></i> Salir</a></li>
                        </ul>
                    </div>
                </div>

                <?php
            } else {
                ?>
                <div class="pull-right login">
                    <form class="form-inline" action="<?= site_url('user/login'); ?>" method="POST">
                        <input type="text" name="name" class="input-small" placeholder="Usuario">
                        <input type="password" name="pass" class="input-small" placeholder="Password">
                        <label class="checkbox">
                            <input type="checkbox" name="rememberme"> Recordarme!
                        </label>
                        <button type="submit" value="entrar" class="btn btn-primary">Entrar</button>
                    </form>
                    <p><a href="<?= site_url("register/form"); ?>">¿Aún no tienes cuenta?</a> <a href="<?= site_url("register/lostPassword"); ?>">¿Has olvidado tu password?</a></p>
                </div>
                <?php if (isset($login_error)) { ?>
                    <div class="pull-right login error lead ">
                        <strong>Nombre de Usuario o Pass incorrectos</strong>
                    </div>
                <?php } ?>
            <?php } ?>


        </div><!-- fin cabecera-->
