<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div class="fieldset fieldset-register">

    <form class="form-horizontal" method="post" action="<?= site_url("user/editProfile"); ?>">
        <fieldset>
            <legend class="formLegend">Edición de perfil</legend>
            <div class="control-group">
                <label class="control-label" for="username">Usuario</label>
                <div class="controls">
                    <input type="text" readonly="true" class="input-xlarge" id="username" name="username" value="<?php echo set_value('username') != null ? set_value('username') : $username; ?>">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="pass">Nueva Password</label>
                <div class="controls">
                    <input type="password" class="input-xlarge" id="pass" name="pass">
                    <?php echo form_error('pass'); ?>
                    <p class="help-block">Tu contraseña. La guardamos encriptada, no te preocupes!</p>
                </div>
            </div>


            <div class="control-group">
                <label class="control-label" for="confirmPassword">Repite Password</label>
                <div class="controls">
                    <input type="password" class="input-xlarge" id="confirmPassword" name="confirmPassword">
                    <?php echo form_error('confirmPassword'); ?>
                    <p class="help-block"></p>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="email">Email</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="email" name="email">
                    <?php echo form_error('email'); ?>
                    <p class="help-block">Importante! A esta cuenta te mandaremos una nueva password si la pierdes.</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="name">Sobre tí</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="description" name="description" value="<?php echo set_value('description'); ?>">
                    <?php echo form_error('description'); ?>
                    <p class="help-block">Si quieres, cuéntanos algo sobre quién eres</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="name">Avatar</label>
                <div class="controls">
                    <input type="file" class="input-xlarge" id="avatar" name="avatar">
                    <?php echo form_error('avatar'); ?>
                </div>
            </div>


            <div class="text-center">
                <input type="submit" class="btn btn-primary" value="Actualizar perfil"></input>
            </div>



        </fieldset>
    </form>
</div><!--/span-->
<?php
require ('application/views/tiles/footer.php');
?>   