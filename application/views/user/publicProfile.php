<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div id="profile" class="wiki-hero-unit">

    <div class="row">
        <div class="span2"><img src="<?= $this->config->item('base_url') ?>resources/img/avatar.jpg" height="150px" width="150px" class="img-rounded"></div>
        <div class="span7">
            <h3><?= $user['USERNAME'] ?></h3>
            <?php
            $userDate = $user['REGISTER_DATE'];
            if ($userDate == null) {
                $userDate = 'WikiTabBook v1';
            }
            ?>
            <h5>En WikiTabBook desde: <?= $userDate ?></h5>
            <h5>Colaboraciones: <?= sizeof($collaborations) ?></h5>
        </div>
    </div>



    <div class="accordion" id="accordionUser">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionUser" href="#collapseAboutMe">
                    Sobre mí
                </a>
            </div>
            <div id="collapseAboutMe" class="accordion-body collapse in">
                <div class="accordion-inner">
                    <?php if($user['DESCRIPTION']!=''){ ?>
                    <p class="lead"><?= $user['DESCRIPTION'] ?></p>
                    <?php } else {?>
                    <p class="lead">Este usuario aún no ha escrito nada sobre él.</p>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionUser" href="#collapseMyCollabs">
                    Colaboraciones
                </a>
            </div>
            <div id="collapseMyCollabs" class="accordion-body collapse">
                <div class="accordion-inner">
                    <?php if (sizeof($collaborations) > 0) { ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width:100px;">Fecha Revisión</th>
                                    <th style="width:150px;">Canción</th>
                                    <th>Comentario</th>
                                </tr>
                            </thead>
                            <tbody

                                <?php
                                foreach ($collaborations as $collab) {
                                    echo '<tr>';
                                    echo '<td>' . $collab['REVISION_DATE'] . '</td>';
                                    echo '<td><a href="' . site_url('tab/show/' . urlencode($collab['SONG_TITLE'])) . '/' . $collab['TAB_ID'] . '#">' . $collab['SONG_TITLE'] . '</a></td>';
                                    echo '<td>' . $collab['TAB_REV_COMMENT'] . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                        </tbody>
                    </table>
                <?php } else { ?>
                    <p>Aún no tiene colaboraciones</p>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="accordion-group">
        <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionUser" href="#collapseMyFavs">
                Favoritas
            </a>
        </div>
        <div id="collapseMyFavs" class="accordion-body collapse">
            <div class="accordion-inner">
                <?php if (sizeof($favs) > 0) { ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width:150px;">Canción</th>
                            </tr>
                        </thead>
                        <tbody

                            <?php
                            foreach ($favs as $fav) {
                                echo '<tr>';
                                echo '<td><a href="' . site_url('tab/show/' . urlencode($fav['SONG_TITLE'])) . '/' . $fav['TAB_ID'] . '#">' . $fav['SONG_TITLE'] . '</a></td>';
                                echo '</tr>';
                            }
                            ?>
                    </tbody>
                </table>
            <?php } else { ?>
                <p>Aún no tiene favoritas</p>
            <?php } ?>
        </div>
    </div>
</div>
</div>

</div>  

<?php
require ('application/views/tiles/footer.php');
?>   