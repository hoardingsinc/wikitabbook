<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div class="mainContent">

    <div class="span8">
        <div class="fieldset fieldset-register">

            <form class="form-horizontal" method="post" action="<?= site_url("user/login"); ?>">
                <fieldset>
                    <legend class="formLegend">Login</legend>

                    <div class="loginFormText error">
                        <?= $this->session->userdata['login_error']; ?>
                        <?php $this->session->set_userdata('login_error', null); ?>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name">Usuario</label>
                        <div class="controls">
                            <input type="text" class="input-xlarge" id="name" name="name" placeholder="Usuario">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="pass">Password</label>
                        <div class="controls">
                            <input type="password" class="input-xlarge" id="pass" name="pass" placeholder="Password">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="rememberme">Recordarme</label>
                        <div class="controls">
                            <input type="checkbox" name="rememberme"/>
                        </div>
                    </div>

                    <div class="loginFormText"> 
                        <p><a href="<?= site_url("register/form"); ?>">¿Aún no tienes cuenta?</a> <a href="<?= site_url("register/lostPassword"); ?>">¿Has olvidado tu password?</a></p>
                    </div>

                    <div class="text-center">
                        <button type="submit" value="entrar" class="btn btn-primary">Entrar</button>


                    </div>

                </fieldset>
            </form>
        </div><!--/span-->
    </div>
</div>
<?php
require ('application/views/tiles/footer.php');
?>   