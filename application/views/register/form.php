<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div class="fieldset fieldset-register">

    <form class="form-horizontal" method="post" action="<?= site_url("register/signup"); ?>">
        <fieldset>
            <legend class="formLegend">Alta de Usuario</legend>
            <div class="control-group">
                <label class="control-label" for="name">Usuario</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="name" name="name" value="<?php echo set_value('name'); ?>">
                    <?php echo form_error('name'); ?>
                    <p class="help-block">Es el nombre que será visible y con el que te loguearás</p>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="pass">Password</label>
                <div class="controls">
                    <input type="password" class="input-xlarge" id="pass" name="pass">
                    <?php echo form_error('pass'); ?>
                    <p class="help-block">Tu contraseña. La guardamos encriptada, no te preocupes!</p>
                </div>
            </div>


            <div class="control-group">
                <label class="control-label" for="confirmPassword">Repite Password</label>
                <div class="controls">
                    <input type="password" class="input-xlarge" id="confirmPassword" name="confirmPassword">
                    <?php echo form_error('confirmPassword'); ?>
                    <p class="help-block"></p>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="email">Email</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="email" name="email">
                    <?php echo form_error('email'); ?>
                    <p class="help-block">Importante! A esta cuenta te mandaremos una nueva password si la pierdes.</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="name">Sobre tí</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="description" name="description" value="<?php echo set_value('description'); ?>">
                    <?php echo form_error('description'); ?>
                    <p class="help-block">Si quieres, cuéntanos algo sobre quién eres</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="email">Captcha de seguridad</label>

                <div class="controls">
                    <input type="text" class="input-xlarge" id="captcha" name="captcha">
                    <?php echo form_error('captcha'); ?>
                    <p class="help-block">Por seguridad, inserta la palabra que lees a continuación.</p>
                    <?= $captcha ?>
                </div>
            </div>

            <div class="text-center">
                <input type="submit" class="btn btn-primary" value="Registrarse"></input>
            </div>



        </fieldset>
    </form>
</div><!--/span-->
<?php
require ('application/views/tiles/footer.php');
?>   