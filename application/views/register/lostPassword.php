<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div class="fieldset fieldset-register">


    <form class="form-horizontal" method="post" action="<?= site_url("register/sendRandomPassword"); ?>">
        <fieldset>
            <legend class="formLegend">Indícanos el email para mandarte una nueva password</legend>


            <div class="control-group">
                <label class="control-label" for="email">Email</label>
                <div class="controls">

                    <input type="text" class="input-xlarge" id="email" name="email">
                    <?php
                    if (isset($error_msg)) {
                        echo '<span class="help-inline error">' . $error_msg . '</span>';
                    };
                    ?>
                    <p class="help-block">Tiene que ser la cuenta con la que te registraste. Te mandamos una nueva password aleatoria y después podrás cambiarla desde tu página personal.</p>
                </div>
            </div>
            <div class="text-center">
                <input type="submit" class="btn btn-primary" value="Enviar"></input>
            </div>
        </fieldset>
    </form>
</div><!--/span-->
<?php
require ('application/views/tiles/footer.php');
?>   