<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>

<div class="row">
    <div class="span8">
        <div class="wiki-hero-unit" style="height: 300px;">
            <h2>Bienvenido a WikiTabBook!</h2>
            <p>WikiTabBook es una aplicación web para realizar transcripciones de canciones de forma colaborativa, 
                así que siéntete libre de participar para añadir o modificar las tablaturas. Puedes leer más en la ayuda</p>
            <p>Actualmente están disponibles <?= $numTabs ?> tabs.</p>
            <p>A continuación puedes buscar entre los tabs, artistas o discos.</p>
            <form class="form-search-center" action="<?= site_url('home/search') ?>" method="get">

                <input type="text" name="search" placeholder="Título, artista..." class="input-medium search-query">
                <button type="submit" class="btn btn-primary"><i class="icon-search"></i> Buscar!</button>
            </form>
        </div>
    </div>
    <div class="span4 firstRightBanner">
        <div class="wiki-hero-unit" style="height: 300px;">
            <h3 class="text-center">Los más colaboradores</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Usuarios</th>
                        <th>Colaboraciones</th>               
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($users as $user) {
                        echo '<tr>';
                        echo '<td><a href="' . site_url('user/publicProfile/' . urlencode($user['USERNAME'])) . '">' . $user['USERNAME'] . '</a></td>';
                        echo '<td>' . $user['NUM_COLLABORATIONS'] . '</td>';

                        echo '</tr>';
                    }
                    ?>

                </tbody>
            </table>

        </div>
    </div>
</div>
<hr />
<div class="row">
    <div class="span8">
        <div class="wiki-hero-unit">
            <h2>Últimos Nuevos Tabs</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width:100px;">Artista</th>
                        <th style="width:150px;">Canción</th>
                    </tr>
                </thead>
                <tbody

                    <?php
                    foreach ($lastFive as $last) {
                        echo '<tr>';
                        echo '<td><a href="' . site_url('artist/show/' . urlencode($last['ARTIST_NAME']) . '/' . $last['ARTIST_ID']) . '">' . $last['ARTIST_NAME'] . '</a></td>';
                        echo '<td><a href="' . site_url('tab/show/' . urlencode($last['SONG_TITLE'])) . '/' . $last['TAB_ID'] . '#">' . $last['SONG_TITLE'] . '</a></td>';
                        echo '</tr>';
                    }
                    ?>
            </tbody>
        </table>
        <div class="text-center">
            <a  href="<?= site_url('home/lastChanges') ?>" class="btn btn-primary btn-large">Ver 50 últimas revisiones</a>
        </div>
    </div>
</div><!--/span-->
<div class="span4" >
    <div class="wiki-hero-unit">
       <a class="twitter-timeline"  href="https://twitter.com/WikiTabBook"  data-widget-id="301607469136879616">Tweets por @WikiTabBook</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

    </div>
</div><!--/span-->
</div><!--/row-->


<?php
require ('application/views/tiles/footer.php');
?>   