<?php
$config = array(
           'register/signup' => array(
                                    array(
                                            'field' => 'name',
                                            'label' => 'Usuario',
                                            'rules' => 'trim|required|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'pass',
                                            'label' => 'Password',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'confirmPassword',
                                            'label' => 'Confirmar password',
                                            'rules' => 'required|matches[pass]'
                                         ),
                                    array(
                                            'field' => 'captcha',
                                            'label' => 'Captcha',
                                            'rules' => 'callback_captchaCheck'
                                    ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'trim|required|valid_email|is_unique[USER.EMAIL]|max_length[50]'
                                         )
                                    ),
            'register/updatePass' => array(
                                    array(
                                            'field' => 'pass',
                                            'label' => 'Password',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'confirmPassword',
                                            'label' => 'Confirmar password',
                                            'rules' => 'required|matches[pass]'
                                         )
                                    ),
         'tab/create' => array(
                                    array(
                                            'field' => 'songTitle',
                                            'label' => 'Título',
                                            'rules' => 'trim|required|max_length[255]'
                                         )
                                    )
               );
?>