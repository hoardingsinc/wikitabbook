<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 *Clase interna para visualizar un acorde a partir de su figura.
 * @package chordpro
 */
class Chord {

  private $chordName = null;
  private $baseFret=null;

  private $stringFret = null;

  private $guitarStrings=null;

  private $chordInHtml='';
  
  private $alreadyParsed = false;

    function __construct($params)
    {
     $chordInfo = $params['chordInfo'];
       $this->chordName = $chordInfo[0];

    $this->baseFret = $chordInfo[2];

    $this->stringFret = array();
    
    $pos = 9;
    for ($i = 1; $i <= 6; $i++) {
      $this->stringFret[$i] = $chordInfo[$pos];
      $pos--;
    }
    
    $this->guitarStrings = array();
    $this->setFrets();

    for ($i = 0 ;$i<6;$i++){
      array_push($this->guitarStrings, GUITAR_STRING);
    }

    }


  /**
   * Genera el código HTML a partir de los datos recogidos
   * en el constructor.
   *
   * @return Acorde en HTML
   */
  public function parseChordToHtml($class, $display){
      
    if (!$this->alreadyParsed) {
      $this->alreadyParsed = true;
      
      for ($i = 1; $i <= 6; $i++) {
        if ($this->stringFret[$i] == 'x')
          $marker = 'x';
        else
          $marker = 'o';
        $this->guitarStrings[$i]{constant('FRET'.$this->stringFret[$i])} = $marker;
        
      }
    
      for($i = 0 ; $i<count($this->guitarStrings);$i++){
        $this->chordInHtml.=$this->guitarStrings[$i]."\r";
      }
    }
    
    return '<div class="'.$class.'" id="'.$this->chordName.
        '"  style="display:'.$display.';"><pre>'
        .$this->chordName . "\n"
        .$this->chordInHtml.'</pre></div>';
  }

  /**
   * Inicializa las "cuerdas" del dibujo de la guitarra y sus trastes
   *
   */
  private function setFrets(){
    if ($this->baseFret == 0)
      $firstFret = 1;
    else
      $firstFret = $this->baseFret;
    $numFrets = 5;
      
    $this->guitarStrings[0]= '  ';
    for ($i = $firstFret; $i < $firstFret+$numFrets; $i++)
      $this->guitarStrings[0] .= $i . '   ';
  }
}
?>
