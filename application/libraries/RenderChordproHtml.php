<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

define('soc','<div class="soc">');
define('eoc','</div>');
define('start_of_chorus','<div class="soc">');
define('end_of_chorus','</div>');
define('sot','<div class="sot">');
define('eot','</div>');
define('start_of_tab','<div class="sot">');
define('end_of_tab','</div>');

define ('GUITAR_STRING', '|---|---|---|---|---|');
define ('FRET0', '0');
define ('FRET1','2');
define ('FRET2','6');
define ('FRET3','10');
define ('FRET4','14');
define ('FRET5','18');
define ('FRETx' , '0');
include APPPATH . 'libraries/Chord.php';
/**
 * Clase que pasa un Tab en ChordPro a un texto HTML
 * para su correcta visualización
 * @package chordpro
 */
class WTB_RenderChordproHtml {
  
/**
 * Indica si hay una directiva que sólo signifique cambio de formato
 * y no texto de salida.
 */
private $thereAreDirectiveNotText = false;
private $isChordDefinitionLine = false;
private $prev = false;
private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model('chordDefinitionBO');
        
        $this->linesArray = array();
        $this->userChordDefinitions = array();

    }
  /**
   * Pasa de un Tab en Chord Pro a código HTML listo 
   * para ser visualizado.
   *
   * @param String $tab Tab
   * @return String Tab en HTML 
   */
  public function parseChordpro($tab){
   
    $this->linesArray = explode("\r",$tab);
    if(sizeof($this->linesArray)==1)
    {
        $this->linesArray = explode("\n", $tab);
    }
    return $this->parseChordproArray($this->linesArray,false);

  }
  
 public function parseChordproArray($tab,$prev){
$this->prev = $prev;
 	$this->linesArray = $tab;
 	 $this->chordsArray = array();
    $this->chordsFound = array();
 	    $numLines = count($this->linesArray);
	$undefinedChords = '';
    $this->result.='<pre>'."\r";
    for($i=0;$i<$numLines;$i++)
    {
      $this->currentLine = $this->linesArray[$i];
      $this->resetStrings();
      $this->parseLine();
    }
    $this->result.='</pre>';
    
    $chordDefinitions =  $this->CI->chordDefinitionBO->getChords();
   //   $chordDefinitions = array();
    for ($i = 0;$i< sizeof($this->chordsFound);$i++)
    {
      if(isset($this->userChordDefinitions[$this->chordsFound[$i]]))
      {
        $chordInfo=$this->userChordDefinitions[$this->chordsFound[$i]];
        $this->result.= $this->defineChord($chordInfo);
      }
      else if(isset($chordDefinitions[$this->chordsFound[$i]]))
      {
        $chordInfo=$chordDefinitions[$this->chordsFound[$i]];
        $this->result.= $this->defineChord($chordInfo);
      }
      else {
      	if($undefinedChords=='')
      	{
      		$undefinedChords.=' '.$this->chordsFound[$i];
      	}
      	else {
      		$undefinedChords.=', '.$this->chordsFound[$i];
      	}
      }
    }
    if($undefinedChords!='')
    {
    	$this->result.='<p>';
      $this->result.='Los siguientes acordes no tienen definici&oacute;n:  '.$undefinedChords;
      $this->result.='</p>';
    }
    return $this->result;
 }

  /**
   * Ver clase base
   */
  private function parseLine(){
    $this->thereAreChords = false;
    $this->thereAreDirectiveNotText = false;
    $this->isChordDefinitionLine = false;
    
    $this->posInChordLine = 1;
    $this->posInLetterLine = 0;
    
    for($this->currentChar = 0; 
        $this->currentChar < strlen($this->currentLine); 
        $this->currentChar++ ) {

      if($this->currentLine{$this->currentChar} == '[')
      {
        $this->setChord();        
      }
      else if($this->currentLine{$this->currentChar} == '{')
      {
        $this->setDirective();
      }
      else
      {
        $this->letterString.=$this->currentLine{$this->currentChar};
        if ($this->isNewUTF8Character($this->currentLine{$this->currentChar}))
          $this->posInLetterLine++;
        if($this->prev)
        {
           if ($this->posInChordLine <= $this->posInLetterLine) {
             $this->chordsString.=' ';
             $this->posInChordLine++;
           }
        }
        else{
           if ($this->posInChordLine < $this->posInLetterLine) {
             $this->chordsString.=' ';
             $this->posInChordLine++;
           }
        }
      }
    }

    if  ($this->isChordDefinitionLine)
      return;
    
    if($this->thereAreChords)
    {
      $this->result.=$this->chordsString;
      $this->result.="\r";
      
      $this->result.=$this->letterString;
      $this->result.="\r";
    } else{
      $this->result.=$this->letterString;
      if(!$this->thereAreDirectiveNotText)
        $this->result.="\r";
    }
  }
  
  // New characters in UTF-8 have the form 10xxx xxxx (in binary)
  private function isNewUTF8Character($char) {
    $charCod = ord($char);
    if ((($charCod & 0x80) == 0x80) && (($charCod & 0x40) == 0x40))
      return false;
    return true;
  }

  /**
   * Ver clase base
   */
  private function setChord(){
    $this->thereAreChords = true;
    //Pasamos el primer corchete
    $this->currentChar++;
    
    
    $chordName= "";
    while($this->currentLine{$this->currentChar} != ']')
    {
    	 if($this->currentChar==strlen($this->currentLine))
          {
            return false;
          }
      $chordName.=$this->currentLine{$this->currentChar};
      $this->currentChar++;
      $this->posInChordLine++;
    }
    $this->chordsString.='<a href="#" class="chord" title="Posici&oacute;n del acorde '.$chordName.'">';
    $this->chordsString.=$chordName;
    $this->saveChord($chordName);
    $this->chordsString.='</a>';
  }
  
  /**
   * Ver clase base
   */
  private function setDirective(){
    //Pasamos la primera llave
    $this->currentChar++;

    $directive='';

    while($this->currentLine{$this->currentChar} != ':' 
        && $this->currentLine{$this->currentChar} != '}')
    {
      $directive.=$this->currentLine{$this->currentChar};
      $this->currentChar++;
      if($this->currentChar==strlen($this->currentLine))
        return false; 
    }


    if($this->currentLine{$this->currentChar} == '}')
    {
      //Marcamos inicio de directiva
      $this->letterString.= constant($directive);
      $this->thereAreDirectiveNotText = true;
    }
    else
    {
      //Nos saltamos los :
      $this->currentChar++;

      if(strcasecmp($directive, 'define')==0)
      {
        $this->saveChordDefinition();
        $this->thereAreDirectiveNotText=true;
                                $this->isChordDefinitionLine = true;
      }
      else
      {
        $this->letterString.= '<span class="'.$directive.'">';
        //Si es del tipo {directiva:xxxx}, leemos el texto y lo sacamos en un <span> con class="directiva"
        while($this->currentLine{$this->currentChar}!= '}')
        {
        	 if($this->currentChar==strlen($this->currentLine))
          {
            $this->letterString.='</span>';
            return false;
          }
          $this->letterString.=$this->currentLine{$this->currentChar};
          $this->currentChar++;
        }
        $this->letterString.='</span>';
      }
    }
  }

  /**
   * Método privado que guarda un acorde encontrado sólo si 
   * no está repetido.
   *
   * @param String Nombre del acorde
   */
  private function saveChord($chordName)
  {
    foreach ($this->chordsFound as $chord)
    {
      if($chord == $chordName)
        return;
    }
    $this->chordsFound[]=$chordName;
  }
  
  /**
   * Método que guarda la definición de un acorde encontrado 
   * en la base de datos.
   * El acorde lo consigue a partir de la línea y caracter actual.
   */
  private function saveChordDefinition()
  {
    $chordInfoToSplit = substr($this->currentLine,$this->currentChar,
                (strlen($this->currentLine)- $this->currentChar - 1));
    $chordInfoToSplit = trim($chordInfoToSplit);
    $chordInfo = array();
    $chordInfo = explode(' ',$chordInfoToSplit);
    $chordInfo[sizeof($chordInfo)-1] = null; //Quitamos el último corchete
   // $chordDefinitions = $_SESSION[CHORD_DEFINITIONS];
  
  //  if(!isset($chordDefinitions[$chordInfo[0]]))
   // {
    //  $chordDefinition = new ChordDefinition();
  //    $chordDefinition->setChordName($chordInfo[0]);
   //   $chordDefinition->setDefinition($chordInfoToSplit);
   //   $chordDefinition->insert();   
  //  }
    
    $this->userChordDefinitions[$chordInfo[0]] = $chordInfoToSplit;
    $this->currentChar = strlen($this->currentLine)- 1 ;
  }

  /**
   * Ver clase base
   */
  private function defineChord($chordDefinition){
    $chordInfo = explode(' ',$chordDefinition);
    $params = array('chordInfo'=>$chordInfo);
   
    $newchord = new Chord($params );

    return $newchord->parseChordToHtml('chordDescription', 'none').$newchord->parseChordToHtml('staticChordDescription', 'block');

  }

  /**
   * Resetea las cadenas de salida para letra y acordes.
   */
  private function  resetStrings(){
    $this->chordsString = '';
    $this->letterString = '';
  }


}
?>