<?php
require_once(SERVER.APP.'libs/php/tcpdf/tcpdf.php');
require_once(SERVER.APP.'chordpro/renderChordpro.php');
define ('GUITAR_STRING', '|---|---|---|---|---|');
define ('FRET0', '0');
define ('FRET1','2');
define ('FRET2','6');
define ('FRET3','10');
define ('FRET4','14');
define ('FRET5','18');
define ('FRETx' , '0');

/**
 * Clase que pasa un Tab en ChordPro a un fichero PDF
 * para su correcta visualización
 * @package chordpro
 */
class RenderChordproPDF extends  renderChordpro {

  private $thereAreDirective = false;
  
  /**
   * Instancia de la clase TCPDF para generar el flujo PDF.
   * @var tcpdf Instancia de la clase TCPDF
   */
  private $pdf ;


  /**
   * Constructor. Inicializa la instancia de TCPDF
   *
   * @param tcpdf $tcpdf Instancia de la claes TCPDF
   */
  public function RenderChordproPDF($tcpdf){
    $this->linesArray = array();
    
    $this->pdf = $tcpdf;

  }

  /**
   * Pasa de un Tab en Chord Pro a fichero PDF listo 
   * para ser visualizado.
   * @param String $tab Tab
   */
  public function parseChordPro($tab){
    $this->chordsArray = array();
    $this->linesArray = explode("\r",$tab);
    $numLines = count($this->linesArray);

    
    for($i=0;$i<$numLines;$i++)
    {
      $this->currentLine = $this->linesArray[$i];
      $this->resetStrings();
      $this->parseLine();
    }
    for ($i = 0;$i< sizeof($this->chordsArray);$i++)
    {
    
      if($this->pdf->GetY()>240)
      {
        $this->pdf->AddPage('P');
      } 
      $this->pdf->Write(3,$this->chordsArray[$i],'',0);
      $this->pdf->Ln(3);
    }
  }


  /**
   * Ver clase base
   */
  public function parseLine(){
    $this->thereAreChords = false;
    $this->thereAreDirective = false;
    for( $this->currentChar=0; $this->currentChar < 
        strlen($this->currentLine); $this->currentChar++ ){
      if($this->currentLine{$this->currentChar} == '[')
      {
        $this->setChord();
        $this->thereAreChords = true;
      }
      else if($this->currentLine{$this->currentChar} == '{')
      {
        $this->setDirective();
        $this->thereAreDirective = true;
      }
      else
      {
        $this->chordsString.=' ';
        $this->letterString.=$this->currentLine{$this->currentChar};
      }
    }


    if($this->thereAreChords == true)
    {
      if($this->pdf->GetY()>255)
      {
        $this->pdf->AddPage('P');
      }
      $this->pdf->SetTextColor('200','90','0');
      $this->pdf->Write(2,$this->chordsString,'',0);
      $this->pdf->Ln(3);
      
      $this->pdf->SetTextColor('0','0','0');
      $this->pdf->Write(1,$this->letterString,'',0);
      $this->pdf->Ln(5);
    }
    else if($this->thereAreDirective)
    {
      
    }
    else{
      
        $this->pdf->Write(1,$this->letterString,'',0);
        $this->pdf->Ln(5);
      
    }
  }
  

  /**
   * Ver clase base
   */
  
  public function setChord(){
    //Pasamos el primer corchete
    $this->currentChar++;
    while($this->currentLine{$this->currentChar} != ']')
    {
      $this->chordsString.=$this->currentLine{$this->currentChar};
      $this->currentChar++;
    }
  }
  
  /**
   * Ver clase base
   */
  public function setDirective(){
    //Pasamos la primera llave
    $this->currentChar++;

    $directive='';

    while($this->currentLine{$this->currentChar} != ':' 
        && $this->currentLine{$this->currentChar} != '}')
    {
      $directive.=$this->currentLine{$this->currentChar};
      $this->currentChar++;
    }

    $directive = trim($directive);
    
    if($this->currentLine{$this->currentChar} == '}')
    {
      
      
      if($directive=='soc' || $directive=='start_of_chorus')
        $this->pdf->SetLeftMargin(20);
      else if($directive=='eoc' || $directive=='end_of_chorus')
        $this->pdf->SetLeftMargin(15);
    
      else if($directive=='sot' || $directive=='start_of_tab')
        $this->pdf->SetFont("freemono", "B", 10);
      else if($directive=='eot' || $directive=='end_of_tab')
        $this->pdf->SetFont("freemono", "", 10);
    }
    else
    {
      //Nos saltamos los :
      $this->currentChar++;

      if(strcasecmp($directive, 'define')==0)
      {
        $this->defineChord();
      }
      else
      {
      
        //Si es del tipo {directiva:xxxx}
        if($directive=='title' || $directive=='t')
        {
          $this->pdf->SetFont("freemono", "B", 14);
          
        }
        else if($directive=='subtitle' || $directive=='st')
        {
          $this->pdf->SetFont("freemono", "", 12);
        }
        else if($directive=='comment' || $directive=='c')
        {
          $this->pdf->SetFont("freemono", "I", 9);
        }
        
        
        while($this->currentLine{$this->currentChar}!= '}')
        {
          $this->letterString.=$this->currentLine{$this->currentChar};
        //  $this->pdf->Write(4,$this->currentLine{$this->currentChar},'',0);
          $this->currentChar++;
        }
        
        $this->pdf->SetTextColor('0','0','0');
        $this->pdf->Write(1,$this->letterString,'',0);
        $this->pdf->Ln(5);
        $this->resetStrings();
        $this->pdf->SetFont("freemono", "", 10);
      }
    }

  }

  /**
   * Ver clase base
   */
  public function defineChord(){

    $chordInfoToSplit = substr($this->currentLine,$this->currentChar,
      (strlen($this->currentLine)- $this->currentChar - 1));
    $chordInfoToSplit = trim($chordInfoToSplit);
    $chordInfo = array();
    $chordInfo = explode(' ',$chordInfoToSplit);
    $chord = new Chord($chordInfo);
    //$this->letterString = $chord->parseChordToHtml();
    $this->chordsArray[] = $chord->parseChordToHtml();
    //Esta linea ya esta procesada, ponemos currentChar al final para que procese la siguiente
    $this->currentChar = strlen($this->currentLine)-1;
  }

  /**
   * Resetea las cadenas de salida de letras y acordes.
   */
  private function  resetStrings(){
    $this->chordsString = '';
    $this->letterString = '';
  }
}

/**
 *Clase interna para visualizar un acorde a partir de su figura.
 * @package chordpro
 */
class Chord {

  private $chordName = null;
  private $baseFret=null;

  private $stringFret = null;

  private $guitarStrings=null;
  private $chordInHtml=null;

  /**
   * Constructor. Inicializa el nombre del acorde, 
   * el traste de referencia y las pulsaciones de cada cuerda.
   *
   * @param String Información del acorde sacada a partir del "define"
   */
  public function Chord($chordInfo){

    $this->chordName = $chordInfo[0];

    $this->baseFret = $chordInfo[2];

    $this->stringFret = array();
    
    $pos = 9;
    for ($i = 1; $i <= 6; $i++) {
      $this->stringFret[$i] = $chordInfo[$pos];
      $pos--;
    }
    $this->guitarStrings = array();
    $this->setFrets();

    for ($i = 0 ;$i<6;$i++){
      array_push($this->guitarStrings,GUITAR_STRING);
    }

  }


  /**
   * Genera una cadena de texto con la figura del acorde
   * para ser mostrada con fuente monoespaciada
   *
   * @return Cadena de texto con el acorde
   */
  public function parseChordToHtml(){
    $this->chordInHtml.=$this->chordName."\n";
    for ($i = 1; $i <= 6; $i++) {
      if ($this->stringFret[$i] == 'x')
        $marker = 'x';
      else
        $marker = 'o';
      $this->guitarStrings[$i]{constant('FRET'.$this->stringFret[$i])} = $marker;
    }
    
    for($i = 0 ; $i<=count($this->guitarStrings);$i++){
      $this->chordInHtml.=$this->guitarStrings[$i]."\n";
    }
    return $this->chordInHtml;
  }


  /**
   * Inicializa las "cuerdas" del dibujo de la guitarra y sus trastes
   *
   */
  private function setFrets(){
    if ($this->baseFret == 0)
      $firstFret = 1;
    else
      $firstFret = $this->baseFret;
    $numFrets = 5;
      
    $this->guitarStrings[0]= '  ';
    for ($i = $firstFret; $i < $firstFret+$numFrets; $i++)
      $this->guitarStrings[0] .= $i . '   ';
  }
}

?>
