<?php

require_once('utils/jsmin.php');

$files = glob("resources/js/*.js");
$js = "";
foreach($files as $file) {
    $js .= JSMin::minify(file_get_contents($file));
}

file_put_contents("resources/js/combined.js", $js);

?>
